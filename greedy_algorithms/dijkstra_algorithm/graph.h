#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <climits>
#include <queue>
#include <set>

// + Bidirectional Dijkstra
// + A* (A-star) search

class Graph {
private:
    using Edge = std::pair<int, int>;

    std::vector<std::vector<Edge>> adjList;
public:
    Graph(int numVertices) {
        adjList.resize(numVertices);
    }

    void addEdge(int v, int e, int weight) {
        adjList.at(v).emplace_back(e, weight);
        adjList.at(e).emplace_back(v, weight);
    }

    void removeEdge(int v, int e) {
        auto itv = std::find_if(adjList.at(v).begin(), adjList.at(v).end(), [e] (const Edge& s) { return s.first == e; });
        auto ite = std::find_if(adjList.at(e).begin(), adjList.at(e).end(), [v] (const Edge& s) { return s.first == v; });

        if (itv != adjList[v].end() && ite != adjList[e].end()) {
            adjList[v].erase(itv);
            adjList[e].erase(ite);
        }
    }

    void printGraph() {
        for (int v = 0; v < adjList.size(); ++v) {
            std::cout << "\n" << v << ":";
            for (auto& e : adjList[v]) {
                std::cout << "-> " << e.first << '(' << e.second << ')';
            }
        }
        std::cout << '\n';
    }

    int getSize() {
        return adjList.size();
    }

    const std::vector<Edge>& getEdges(int v) {
        return adjList.at(v);
    }

    std::vector<std::pair<int, int>> dijkstra(int start) {
        int size = adjList.size();
        std::vector<int> distance(size, INT_MAX);
        std::vector<int> previous(size, -1);

        distance.at(start) = 0;

        std::priority_queue<Edge> q;
        q.push(std::make_pair(0, start));

        while (!q.empty()) {
            int v = q.top().second;
            q.pop();

            for (auto& e : getEdges(v)) {
                int u = e.first;
                int weight = e.second;

                int dist = distance[v] + weight;
                if (dist < distance[u]) {
                    distance[u] = dist;
                    previous[u] = v;

                    q.push(std::make_pair(dist, u));
                }
            }
        }

        std::vector<std::pair<int, int>> res(size);
        for (int i = 0; i < size; ++i) {
            res[i] = std::make_pair(previous[i], distance[i]);
        }
        return res;
    }
};

#endif // GRAPH_H
