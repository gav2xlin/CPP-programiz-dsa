#include "graph.h"
#include <iostream>

using namespace std;

int main()
{
    Graph g{6};

    g.addEdge(0, 1, 4);
    g.addEdge(0, 2, 4);
    g.addEdge(1, 2, 2);
    g.addEdge(2, 3, 3);
    g.addEdge(2, 4, 6);
    g.addEdge(2, 5, 1);
    g.addEdge(3, 4, 2);
    g.addEdge(5, 4, 3);

    g.printGraph();

    vector<pair<int, int>> res = g.dijkstra(0);

    cout << "the shortest paths from the source vertex 0\nvertex previous distance\n";
    for (int i = 0; i < res.size(); ++i) {
        cout << i << '\t' << res[i].first << '\t' << res[i].second << '\n';
    }

    return 0;
}
