#include "graph.h"
#include <iostream>

Graph::Graph(int V) {
    parent = new int[V];

    // i: 0 1 2 3 4 5
    // parent[i]: 0 1 2 3 4 5
    for (int i = 0; i < V; i++) {
        parent[i] = i;
    }

    G.clear();
    T.clear();
}

Graph::~Graph() {
    delete[] parent;
}

void Graph::AddWeightedEdge(int u, int v, int w) {
    G.push_back(std::make_pair(w, edge(u, v)));
}

int Graph::find_set(int i) {
    // if i is the parent of itself
    if (i == parent[i]) {
        return i;
    } else {
        // else if i is not the parent of itself
        // then i is not the representative of his set,
        // so we recursively call find_set on its parent
        return find_set(parent[i]);
    }
}

void Graph::union_set(int u, int v) {
    parent[u] = parent[v];
}

void Graph::kruskal() {
    sort(G.begin(), G.end());  // increasing weight

    for (int i = 0; i < G.size(); ++i) {
        int uRep = find_set(G[i].second.first);
        int vRep = find_set(G[i].second.second);

        if (uRep != vRep) {
            T.push_back(G[i]);  // add to tree
            union_set(uRep, vRep);
        }
    }
}

void Graph::print() {
    std::cout << "Edge :" << " Weight" << std::endl;

    for (int i = 0; i < T.size(); i++) {
        std::cout << T[i].second.first << " - " << T[i].second.second << " : " << T[i].first << std::endl;
    }
}
