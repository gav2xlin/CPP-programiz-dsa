#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include <utility>

class Graph {
private:
    using edge = std::pair<int, int>;

    std::vector<std::pair<int, edge> > G;  // graph
    std::vector<std::pair<int, edge> > T;  // mst (minimal spanning tree)

    int *parent;
    int V;  // number of vertices/nodes in graph

    int find_set(int i);
    void union_set(int u, int v);
public:
    Graph(int V);
    ~Graph();

    void AddWeightedEdge(int u, int v, int w);
    void kruskal();
    void print();
};
#endif // GRAPH_H
