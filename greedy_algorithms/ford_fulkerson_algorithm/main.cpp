#include <iostream>
#include <queue>
#include <climits>
#include <cstring>

using namespace std;

#define V 6

bool bfs(int rGraph[V][V], int s, int t, int parent[]) {
    bool visited[V];
    memset(visited, 0, sizeof(visited));

    visited[s] = true;
    parent[s] = -1;

    queue<int> q;
    q.push(s);

    while (!q.empty()) {
        int u = q.front();
        q.pop();

        for (int v = 0; v < V; ++v) {
            if (!visited[v] && rGraph[u][v] > 0) {
                visited[v] = true;
                parent[v] = u;

                q.push(v);
            }
        }
    }

    return visited[t];
}

int ford_fulkerson(int graph[V][V], int s, int t) {
    int rGraph[V][V];
    for (int u = 0; u < V; ++u) {
        for (int v = 0; v < V; ++v) {
            rGraph[u][v] = graph[u][v];
        }
    }

    int parent[V];
    int max_flow = 0;

    // updating the residual values of edges
    while (bfs(rGraph, s, t, parent)) {
        int path_flow = INT_MAX;

        for (int v = t; v != s; v = parent[v]) {
            int u = parent[v];

            path_flow = min(path_flow, rGraph[u][v]);
        }

        for (int v = t; v != s; v = parent[v]) {
            int u = parent[v];

            rGraph[u][v] -= path_flow;
            rGraph[v][u] += path_flow;
        }

        // adding the path flows
        max_flow += path_flow;
    }

    return max_flow;
}

int main() {
    int graph[V][V] = {{0, 8, 0, 0, 3, 0},
                       {0, 0, 9, 0, 0, 0},
                       {0, 0, 0, 0, 7, 2},
                       {0, 0, 0, 0, 0, 5},
                       {0, 0, 7, 4, 0, 0},
                       {0, 0, 0, 0, 0, 0}};

    cout << "Max Flow: " << ford_fulkerson(graph, 0, 5) << endl;

    return 0;
}
