#include "graph.h"
#include <iostream>

using namespace std;

int main()
{
    Graph g{7};

    g.addEdge(0, 1, 5);
    g.addEdge(0, 2, 4);
    g.addEdge(1, 3, 3);
    g.addEdge(2, 1, 6);
    g.addEdge(3, 2, 2);

    g.printGraph();

    vector<pair<int, int>> res = g.bellman_ford(0);

    cout << "the shortest paths from the source vertex 0\nvertex previous distance\n";
    for (int i = 0; i < res.size(); ++i) {
        cout << i << '\t' << res[i].first << '\t' << res[i].second << '\n';
    }

    return 0;
}
