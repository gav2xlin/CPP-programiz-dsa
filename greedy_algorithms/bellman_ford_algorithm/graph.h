#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <climits>
#include <stdexcept>

class Graph {
private:
    using Edge = std::pair<int, int>;

    std::vector<std::vector<Edge>> adjList;
public:
    Graph(int numVertices) {
        adjList.resize(numVertices);
    }

    void addEdge(int v, int e, int weight) {
        adjList.at(v).emplace_back(e, weight);
    }

    void removeEdge(int v, int e) {
        auto it = std::find_if(adjList.at(v).begin(), adjList.at(v).end(), [e] (const Edge& s) { return s.first == e; });

        if (it != adjList[v].end()) {
            adjList[v].erase(it);
        }
    }

    void printGraph() {
        for (int v = 0; v < adjList.size(); ++v) {
            std::cout << "\n" << v << ":";
            for (auto& e : adjList[v]) {
                std::cout << "-> " << e.first << '(' << e.second << ')';
            }
        }
        std::cout << '\n';
    }

    int getSize() {
        return adjList.size();
    }

    const std::vector<Edge>& getEdges(int v) {
        return adjList.at(v);
    }

    std::vector<std::pair<int, int>> bellman_ford(int start) {
        int size = adjList.size();
        std::vector<int> distance(size, INT_MAX); // positive infinity
        std::vector<int> previous(size, -1);

        distance.at(start) = 0;

        // relax edges V - 1 times
        for (int v = 0; v < adjList.size(); ++v) {
            for (auto& e : adjList[v]) {
                int u = e.first, weight = e.second;

                if (distance[v] != INT_MAX && distance[v] + weight < distance[u]) {
                    distance[u] = distance[v] + weight;
                    previous[u] = v;
                }
            }
        }

        // detect negative cycle
        for (int v = 0; v < adjList.size(); ++v) {
            for (auto& e : adjList[v]) {
                int u = e.first, weight = e.second;

                if (distance[v] != INT_MAX && distance[v] + weight < distance[u]) {
                    // distance[u] = INT_MIN; // negative infinity
                    // previous[u] = -1;
                    throw std::invalid_argument("Graph contains negative cycle");
                }
            }
        }

        std::vector<std::pair<int, int>> res(size);
        for (int i = 0; i < size; ++i) {
            res[i] = std::make_pair(previous[i], distance[i]);
        }
        return res;
    }
};

#endif // GRAPH_H
