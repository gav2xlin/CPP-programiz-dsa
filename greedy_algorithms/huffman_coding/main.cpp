#include <iostream>
#include <algorithm>

using namespace std;

#define MAX_TREE_HT 50

struct MinHNode {
    unsigned freq;
    char item;
    struct MinHNode *left, *right;
};

struct MinH {
    unsigned size;
    unsigned capacity;
    struct MinHNode **array;
};

// creating Huffman tree node
MinHNode *newNode(char item, unsigned freq) {
    MinHNode *temp = new MinHNode;

    temp->left = temp->right = nullptr;
    temp->item = item;
    temp->freq = freq;

    return temp;
}

// create min heap using given capacity
MinH *createMinH(unsigned capacity) {
    MinH *minHeap = new MinH;

    minHeap->size = 0;
    minHeap->capacity = capacity;
    minHeap->array = new MinHNode *[minHeap->capacity];

    return minHeap;
}

// print the array
void printArray(int arr[], int n) {
    for (int i = 0; i < n; ++i) {
        cout << arr[i];
    }
    cout << "\n";
}

// heapify
void minHeapify(MinH *minHeap, int idx) {
    int smallest = idx;

    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq) {
        smallest = left;
    }

    if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq) {
        smallest = right;
    }

    if (smallest != idx) {
        swap(minHeap->array[smallest], minHeap->array[idx]);
        minHeapify(minHeap, smallest);
    }
}

// check if size if 1
int checkSizeOne(MinH *minHeap) {
    return minHeap->size == 1;
}

// extract the min
MinHNode *extractMin(MinH *minHeap) {
    MinHNode *temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];

    --minHeap->size;
    minHeapify(minHeap, 0);

    return temp;
}

// insertion
void insertMinHeap(MinH *minHeap, MinHNode *minHeapNode) {
    ++minHeap->size;
    int i = minHeap->size - 1;

    while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq) {
        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }

    minHeap->array[i] = minHeapNode;
}

// build min heap
void buildMinHeap(MinH *minHeap) {
    int n = minHeap->size - 1;

    for (int i = (n - 1) / 2; i >= 0; --i) {
        minHeapify(minHeap, i);
    }
}

int isLeaf(MinHNode *root) {
    return !(root->left) && !(root->right);
}

MinH *createAndBuildMinHeap(char item[], int freq[], int size) {
    MinH *minHeap = createMinH(size);

    for (int i = 0; i < size; ++i) {
        minHeap->array[i] = newNode(item[i], freq[i]);
    }

    minHeap->size = size;
    buildMinHeap(minHeap);

    return minHeap;
}

MinHNode *buildHfTree(char item[], int freq[], int size) {
    MinH *minHeap = createAndBuildMinHeap(item, freq, size);

    while (!checkSizeOne(minHeap)) {
        MinHNode *left = extractMin(minHeap);
        MinHNode *right = extractMin(minHeap);

        MinHNode *top = newNode('$', left->freq + right->freq);

        top->left = left;
        top->right = right;

        insertMinHeap(minHeap, top);
    }

    return extractMin(minHeap);
}

void printHCodes(MinHNode *root, int arr[], int top) {
    if (root->left) {
        arr[top] = 0;
        printHCodes(root->left, arr, top + 1);
    }

    if (root->right) {
        arr[top] = 1;
        printHCodes(root->right, arr, top + 1);
    }

    if (isLeaf(root)) {
        cout << root->item << "  | ";
        printArray(arr, top);
    }
}

// wrapper function
void HuffmanCodes(char item[], int freq[], int size) {
    MinHNode *root = buildHfTree(item, freq, size);

    int arr[MAX_TREE_HT], top = 0;

    printHCodes(root, arr, top);
}

int main() {
    char arr[] = {'A', 'B', 'C', 'D'};
    int freq[] = {5, 1, 6, 3};

    int size = sizeof(arr) / sizeof(arr[0]);

    cout << "Char | Huffman code ";
    cout << "\n----------------------\n";

    HuffmanCodes(arr, freq, size);

    return 0;
}
