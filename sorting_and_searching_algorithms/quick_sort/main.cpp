#include <iostream>
#include <algorithm>
//#include <utility>
#include <vector>

using namespace std;

void printArray(vector<int> &arr) {
    for (auto v : arr) {
        cout << v << ' ';
    }
    cout << endl;
}

int partition(vector<int> &arr, int low, int high) {
    int pivot = arr[high];

    int i = low - 1;

    for (int j = low; j < high; ++j) {
        if (arr[j] <= pivot) {
            swap(arr[++i], arr[j]);
        }
    }

    swap(arr[i + 1], arr[high]);

    return i + 1;
}

/*int partition(vector<int> &arr, int low, int high)
{
    int pivot = arr[low];
    int i = low - 1, j = high + 1;

    while (true) {
        do {
            ++i;
        } while (arr[i] < pivot);

        do {
            --j;
        } while (arr[j] > pivot);

        if (i >= j) {
            return j;
        }

        swap(arr[i], arr[j]);
    }
}*/

void quickSort(vector<int> &arr, int low, int high) {
    if (low < high) {
        int pi = partition(arr, low, high);

        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}

/*pair<int, int> partition(vector<int> &nums, int left, int right) {
    int mid = left;
    int pivot = nums[right];

    while (mid <= right) {
        if (nums[mid] < pivot) {
            swap(nums[left], nums[mid]);
            ++left, ++mid;
        } else if (nums[mid] > pivot) {
            swap(nums[mid], nums[right]);
            --right;
        } else {
            ++mid;
        }
    }

    return make_pair(left - 1, mid);
}

// 3–way Quicksort (Dutch National Flag)
void quickSort(vector<int> &nums, int left, int right) {
    if (left >= right) {
        return;
    }

    if (right - left == 1) {
        if (nums[left] < nums[right]) {
            swap(nums[left], nums[right]);
        }
        return;
    }

    pair<int, int> pivot = partition(nums, left, right);

    quickSort(nums, left, pivot.first);
    quickSort(nums, pivot.second, right);
}*/

int main() {
    vector<int> data{8, 7, 6, 1, 0, 9, 2};

    cout << "Unsorted Array: \n";
    printArray(data);

    quickSort(data, 0, data.size() - 1);

    cout << "Sorted array in ascrighting order: \n";
    printArray(data);

    return 0;
}
