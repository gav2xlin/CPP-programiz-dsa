#include <iostream>
#include <iomanip>

using namespace std;

#define NARRAY 7   // array size
#define NBUCKET 6  // number of buckets
#define INTERVAL 10  // each bucket capacity

struct Node {
    int data;
    Node *next;
};

void BucketSort(int arr[]);
Node *InsertionSort(struct Node *list);
void printArray(int arr[]);
void printBuckets(struct Node *list);
int getBucketIndex(int value);

void BucketSort(int arr[]) {
    int i, j;
    Node **buckets;

    buckets = new Node* [NBUCKET]{nullptr};

    for (i = 0; i < NARRAY; ++i) {
        int pos = getBucketIndex(arr[i]);

        Node *current = new Node;
        current->data = arr[i];
        current->next = buckets[pos];

        buckets[pos] = current;
    }

    for (i = 0; i < NBUCKET; i++) {
        cout << "Bucket[" << i << "] : ";
        printBuckets(buckets[i]);
        cout << endl;
    }

    for (i = 0; i < NBUCKET; ++i) {
        buckets[i] = InsertionSort(buckets[i]);
    }

    cout << "-------------" << endl;
    cout << "Bucktets after sorted" << endl;

    for (i = 0; i < NBUCKET; i++) {
        cout << "Bucket[" << i << "] : ";
        printBuckets(buckets[i]);
        cout << endl;
    }

    for (j = 0, i = 0; i < NBUCKET; ++i) {
        Node *node = buckets[i];
        while (node) {
            arr[j++] = node->data;
            node = node->next;
        }
    }

    for (i = 0; i < NBUCKET; ++i) {
        Node *node = buckets[i];
        while (node) {
            Node *tmp = node;
            node = node->next;
            delete tmp;
        }
    }

    delete[] buckets;
}

Node *InsertionSort(Node *list) {
    if (list == nullptr || list->next == nullptr) {
        return list;
    }

    Node *k, *nodeList;
    nodeList = list;
    k = list->next;
    nodeList->next = 0;

    while (k != nullptr) {
        Node *ptr;
        if (nodeList->data > k->data) {
            Node *tmp = k;
            k = k->next;
            tmp->next = nodeList;
            nodeList = tmp;

            continue;
        }

        for (ptr = nodeList; ptr->next != nullptr; ptr = ptr->next) {
            if (ptr->next->data > k->data) {
                break;
            }
        }

        if (ptr->next != nullptr) {
            Node *tmp= k;
            k = k->next;
            tmp->next = ptr->next;
            ptr->next = tmp;

            continue;
        } else {
            ptr->next = k;
            k = k->next;
            ptr->next->next = nullptr;

            continue;
        }
    }

    return nodeList;
}

int getBucketIndex(int value) {
    return value / INTERVAL;
}

void printArray(int arr[]) {
    for (int i = 0; i < NARRAY; ++i) {
        cout << setw(3) << arr[i];
    }
    cout << endl;
}

void printBuckets(struct Node *list) {
    Node *cur = list;
    while (cur) {
        cout << setw(3) << cur->data;
        cur = cur->next;
    }
}

int main(void) {
    int array[NARRAY] = {42, 32, 33, 52, 37, 47, 51};

    cout << "Initial array: " << endl;
    printArray(array);
    cout << "-------------" << endl;

    BucketSort(array);

    cout << "-------------" << endl;
    cout << "Sorted array: " << endl;
    printArray(array);

    return 0;
}
