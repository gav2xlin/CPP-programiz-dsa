#include <iostream>
#include <vector>
#include <cstring>

using namespace std;

int getMax(vector<int>& arr) {
    int max = arr[0];
    for (int i = 1; i < arr.size(); ++i) {
        if (arr[i] > max) {
            max = arr[i];
        }
    }
    return max;
}

void countingSort(vector<int>& arr, int place) {

    int size = arr.size();
    vector<int> output(size);

    const int max = 10;
    vector<int> count(max, 0);

    auto calc_pos = [place](int num) {
        return (num / place) % 10;
    };

    // store the count of each element
    for (int i = 0; i < size; ++i) {
        ++count[calc_pos(arr[i])];
    }

    // store cumulative sum of the elements
    for (int i = 1; i < max; ++i) {
        count[i] += count[i - 1];
    }

    // find the index of each element of the original array
    for (int i = size - 1; i >= 0; --i) {
        int pos = calc_pos(arr[i]);
        --count[pos];
        output[count[pos]] = arr[i];
    }

    // copy the sorted elements into original array
    for (int i = 0; i < size; ++i) {
        arr[i] = output[i];
    }
}

void radixsort(vector<int>& arr) {
    int max = getMax(arr);

    for (int place = 1; max / place > 0; place *= 10) {
        countingSort(arr, place);
    }
}

void printArray(vector<int>& arr) {
    for (auto v : arr) {
        cout << v << " ";
    }
    cout << endl;
}

int main() {
    vector<int> arr{121, 432, 564, 23, 1, 45, 788};

    radixsort(arr);

    printArray(arr);

    return 0;
}
