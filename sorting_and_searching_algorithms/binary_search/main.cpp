#include <iostream>

using namespace std;

/*int binarySearch(int array[], int x, int low, int high) {
    while (low <= high) {
        //int mid = low + (high - low) / 2;
        int mid = (low + high) / 2;

        if (array[mid] < x) {
            low = mid + 1;
        } else if (array[mid] > x) {
            high = mid - 1;
        } else {
            return mid;
        }
    }

    return -1;
}*/

int binarySearch(int array[], int x, int low, int high) {
    if (high >= low) {
        //int mid = low + (high - low) / 2;
        int mid = (low + high) / 2;

        if (array[mid] < x) {
            return binarySearch(array, x, mid + 1, high); // low = mid + 1;
        } else if (array[mid] > x) {
            return binarySearch(array, x, low, mid - 1); // high = mid - 1;
        } else {
            return mid;
        }
    }

    return -1;
}

int main(void) {
    int array[] = {3, 4, 5, 6, 7, 8, 9};
    //int x = 4; // 1
    //int x = 7; // 4
    int x = 9; // 6
    int n = sizeof(array) / sizeof(array[0]);

    int idx = binarySearch(array, x, 0, n - 1);

    if (idx >= 0) {
        cout << "Element is found at index: " << idx << endl;
    } else {
        cout << "Not found" << endl;
    }

    return 0;
}
