#include <iostream>
#include <cstring>

using namespace std;

void printArray(int array[], int size) {
    for (int i = 0; i < size; i++) {
        cout << array[i] << " ";
    }
    cout << endl;
}

void countingSort(int arr[], int size) {
    // find max
    int k = arr[0];
    for (int i = 1; i < size; i++) {
        if (arr[i] > k) {
            k = arr[i];
        }
    }

    int count[k + 1];
    int output[size];

    memset(count, 0, sizeof(count));

    // store the count of each element
    for (int i = 0; i < size; ++i) {
        ++count[arr[i]];
    }

    // store cumulative sum of the elements
    for (int i = 1; i <= k; ++i) {
        count[i] += count[i - 1];
    }

    // find the index of each element of the original array
    for (int i = size - 1; i >= 0; --i) {
        --count[arr[i]];
        output[count[arr[i]]] = arr[i];
    }

    // copy the sorted elements into original array
    for (int i = 0; i < size; i++) {
        arr[i] = output[i];
    }
}

int main() {
    int array[] = {4, 2, 2, 8, 3, 3, 1};
    int n = sizeof(array) / sizeof(array[0]);

    countingSort(array, n);

    printArray(array, n);

    // 4 2 2 8 3 3 1
    // 0 1 2 2 1 0 0 0 1
    // 0 1 3 5 6 6 6 6 7
    // 1 1 [0] = 1
    // 3 5 [4] = 3
    // 3 4 [3] = 3
    // 8 7 [6] = 8
    // 2 3 [2] = 2
    // 2 2 [1] = 2
    // 4 6 [5] = 4
    // 1 2 2 3 3 4 8

    return 0;
}
