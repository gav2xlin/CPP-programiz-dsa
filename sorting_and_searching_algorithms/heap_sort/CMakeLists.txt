cmake_minimum_required(VERSION 3.5)

project(heap_sort LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(heap_sort main.cpp)

install(TARGETS heap_sort
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
