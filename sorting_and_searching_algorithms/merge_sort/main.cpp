#include <iostream>

using namespace std;

void merge(int arr[], int low, int mid, int high) {
    int ln = mid - low + 1;
    int rn = high - mid;

    int L[ln], R[rn];

    for (int i = 0; i < ln; ++i) {
        L[i] = arr[low + i];
    }
    for (int j = 0; j < rn; ++j) {
        R[j] = arr[mid + 1 + j];
    }

    int i, j, k;
    i = 0;
    j = 0;
    k = low;

    while (i < ln && j < rn) {
        if (L[i] <= R[j]) {
            arr[k] = L[i];
            ++i;
        } else {
            arr[k] = R[j];
            ++j;
        }
        ++k;
    }

    while (i < ln) {
        arr[k] = L[i];
        ++i;
        ++k;
    }

    while (j < rn) {
        arr[k] = R[j];
        ++j;
        ++k;
    }
}

void mergeSort(int arr[], int low, int high) {
    if (low < high) {
        //int mid = low + (high - low) / 2;
        int mid = (low + high) / 2;

        mergeSort(arr, low, mid);
        mergeSort(arr, mid + 1, high);

        merge(arr, low, mid, high);
    }
}

void printArray(int arr[], int size) {
    for (int i = 0; i < size; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

int main() {
    int arr[] = {6, 5, 12, 10, 9, 1};
    int size = sizeof(arr) / sizeof(arr[0]);

    mergeSort(arr, 0, size - 1);

    cout << "Sorted array: \n";
    printArray(arr, size);

    return 0;
}
