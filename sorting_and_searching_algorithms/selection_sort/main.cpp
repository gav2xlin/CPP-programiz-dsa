#include <iostream>
#include <algorithm>

using namespace std;

void printArray(int array[], int size) {
    for (int i = 0; i < size; i++) {
        cout << array[i] << " ";
    }
    cout << endl;
}

void selectionSort(int array[], int size) {
    for (int i = 0; i < size - 1; ++i) {
        int min_idx = i;

        for (int j = i + 1; j < size; ++j) {
            if (array[j] < array[min_idx])
                min_idx = j;
        }

        swap(array[min_idx], array[i]);
    }
}

int main() {
    int data[] = {20, 12, 10, 15, 2};
    int size = sizeof(data) / sizeof(data[0]);

    selectionSort(data, size);

    cout << "Sorted array in Acsending Order:\n";
    printArray(data, size);

    return 0;
}
