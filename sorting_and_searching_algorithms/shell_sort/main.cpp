#include <iostream>

using namespace std;

void shellSort(int arr[], int n) {
    // rearrange elements at each n/2, n/4, n/8, ... intervals
    for (int interval = n / 2; interval > 0; interval /= 2) {
        for (int i = interval; i < n; i += 1) {
            int key = arr[i];
            int j;

            for (j = i; j >= interval && arr[j - interval] > key; j -= interval) {
                arr[j] = arr[j - interval];
            }

            arr[j] = key;
            /*int j = i - interval;

            while (j >= 0 && arr[j] > key) {
                arr[j + interval] = arr[j];
                j -= interval;
            }

            arr[j + interval] = key;*/
        }
    }
}

void printArray(int array[], int size) {
    for (int i = 0; i < size; i++) {
        cout << array[i] << " ";
    }
    cout << endl;
}

int main() {
    int data[] = {9, 8, 3, 7, 5, 6, 4, 1};
    int size = sizeof(data) / sizeof(data[0]);

    shellSort(data, size);

    cout << "Sorted array: \n";
    printArray(data, size);

    return 0;
}
