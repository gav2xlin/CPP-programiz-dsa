#include "binary_tree_node.h"
#include "tree_traversal.h"
#include <iostream>

using namespace std;
using namespace ds;

int main() {
    BinaryTreeNode<int> root(1);
    root.addLeft(2);
    root.addRight(3);
    root.getLeft()->addLeft(4);

    // auto cb = [](auto val) { // using a copy constructor
    auto cb = [](auto& val) {
        cout << val << ' ';
    };

    TreeTraversal<int> traversal;
    cout << "Inorder traversal ";
    traversal.inorderTraversal(root, cb);

    cout << "\nPreorder traversal ";
    traversal.preorderTraversal(root, cb);

    cout << "\nPostorder traversal ";
    traversal.postorderTraversal(root, cb);

    cout << endl;

    // copy constructor

    BinaryTreeNode<int> nc(root);

    cout << "\nCopy constructor ";

    traversal.inorderTraversal(nc, cb);

    cout << endl;

    // copy assignment operator

    BinaryTreeNode<int> n(0);

    cout << "\nBefore copying ";
    traversal.inorderTraversal(n, cb);

    n = root;

    cout << "\nAfter copying ";
    traversal.inorderTraversal(n, cb);

    cout << endl;

    return 0;
}
