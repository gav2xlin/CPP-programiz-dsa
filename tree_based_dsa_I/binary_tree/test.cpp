#include "binary_tree_node.h"
#include "tree_traversal.h"
#include "gtest/gtest.h"
#include "gmock/gmock-matchers.h"
#include <vector>
#include <stdexcept>

namespace ds {
class BinaryTreeTest : public testing::Test {
protected:
    BinaryTreeNode<int>* root;
    TreeTraversal<int> traversal;

    virtual void SetUp() {
        root = new BinaryTreeNode<int>(1);
        root->addLeft(2);
        root->addRight(3);
        root->getLeft()->addRight(4);
    }

    virtual void TearDown() {
        delete root;
    }
};

TEST_F(BinaryTreeTest, PreorderTraversal) {
    std::vector<int> res;
    auto cb = [&res](auto& val) {
        res.push_back(val.getData());
    };
    traversal.preorderTraversal(*root, cb);

    using ::testing::ElementsAreArray;

    EXPECT_THAT(res, ElementsAreArray({1, 2, 4, 3}));
}

TEST_F(BinaryTreeTest, PostorderTraversal) {
    std::vector<int> res;
    auto cb = [&res](auto& val) {
        res.push_back(val.getData());
    };
    traversal.postorderTraversal(*root, cb);

    using ::testing::ElementsAreArray;

    EXPECT_THAT(res, ElementsAreArray({4, 2, 3, 1}));
}

TEST_F(BinaryTreeTest, InorderTraversal) {
    std::vector<int> res;
    auto cb = [&res](auto& val) {
        res.push_back(val.getData());
    };
    traversal.inorderTraversal(*root, cb);

    using ::testing::ElementsAreArray;

    EXPECT_THAT(res, ElementsAreArray({2, 4, 1, 3}));
}

TEST_F(BinaryTreeTest, NodesExist) {
    EXPECT_THROW(root->addLeft(2), std::logic_error);
    EXPECT_THROW(root->addLeft(3), std::logic_error);
}

TEST_F(BinaryTreeTest, RuleViolation) {
    EXPECT_THROW(root->getLeft()->addLeft(5), std::invalid_argument);
    EXPECT_THROW(root->getLeft()->getRight()->addLeft(1), std::invalid_argument);
    EXPECT_THROW(root->getLeft()->getRight()->addRight(1), std::invalid_argument);
}
};
