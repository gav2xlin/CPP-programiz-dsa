#ifndef TREETRAVERSAL_H
#define TREETRAVERSAL_H

#include "binary_tree_node.h"
//#include "gtest/gtest.h"
#include <functional>

namespace ds {
template<typename T>
class TreeTraversal {
public:
    using cb = std::function<void(BinaryTreeNode<T>& node)>;

    void preorderTraversal(BinaryTreeNode<T>& node, cb callback);
    void postorderTraversal(BinaryTreeNode<T>& node, cb callback);
    void inorderTraversal(BinaryTreeNode<T>& node, cb callback);
};
}

#endif // TREETRAVERSAL_H
