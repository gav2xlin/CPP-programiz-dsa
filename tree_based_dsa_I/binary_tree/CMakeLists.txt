cmake_minimum_required(VERSION 3.5)

project(binary_tree LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include(FetchContent)
FetchContent_Declare(
  googletest
  GIT_REPOSITORY https://github.com/google/googletest.git
  GIT_TAG        release-1.12.1
)
# For Windows: Prevent overriding the parent project's compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

set(BINARY_TREE_SOURCES tree_traversal.cpp binary_tree_node.cpp)
set(BINARY_TREE_HEADERS tree_traversal.h binary_tree_node.h)

add_executable(binary_tree_test ${BINARY_TREE_SOURCES} ${BINARY_TREE_HEADERS} test.cpp)
target_link_libraries(binary_tree_test gtest_main gmock)
add_test(NAME binary_tree_test COMMAND binary_tree_test)

add_executable(binary_tree ${BINARY_TREE_SOURCES} ${BINARY_TREE_HEADERS} main.cpp)
target_link_libraries(binary_tree gtest gmock)
