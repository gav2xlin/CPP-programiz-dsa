#ifndef BINARY_TREE_NODE_H
#define BINARY_TREE_NODE_H

#include "gtest/gtest.h"
#include <iostream>
//#include <utility> // std::move

namespace ds {
template <typename T>
class BinaryTreeNode {
private:
    T data;
    BinaryTreeNode *left, *right;

    void deleteAll(BinaryTreeNode *node);
    void copyNodes(BinaryTreeNode* dst, const BinaryTreeNode* src);
public:
    FRIEND_TEST(TreeTraversalTest, PreorderTraversal);
    FRIEND_TEST(TreeTraversalTest, PostorderTraversal);
    FRIEND_TEST(TreeTraversalTest, InorderTraversal);

    BinaryTreeNode(T value);
    ~BinaryTreeNode();

    T getData();
    void setData(T value);

    BinaryTreeNode* getLeft();
    void addLeft(T value);
    void removeLeft();

    BinaryTreeNode* getRight();
    void addRight(T value);
    void removeRight();

    BinaryTreeNode(const BinaryTreeNode &other); // copy constructor
    BinaryTreeNode& operator=(const BinaryTreeNode& other); // copy assignment operator

    //BinaryTreeNode(BinaryTreeNode&& other) noexcept; // move constructor
    //BinaryTreeNode& operator=(BinaryTreeNode&& other) noexcept; // move assignment operator

    // https://en.cppreference.com/w/cpp/language/friend

    friend std::ostream& operator<<(std::ostream& os, const BinaryTreeNode& node) {
        os << node.data;
        return os;
    }
};
}

#endif // BINARY_TREE_NODE_H
