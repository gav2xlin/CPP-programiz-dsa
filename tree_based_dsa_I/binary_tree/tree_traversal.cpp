#include "tree_traversal.h"
#include <string>

namespace ds {
template<typename T>
void TreeTraversal<T>::preorderTraversal(BinaryTreeNode<T>& node, cb callback) {
    callback(node);
    if (node.getLeft() != nullptr) preorderTraversal(*node.getLeft(), callback);
    if (node.getRight() != nullptr) preorderTraversal(*node.getRight(), callback);
}

template<typename T>
void ds::TreeTraversal<T>::postorderTraversal(BinaryTreeNode<T>& node, cb callback) {
    if (node.getLeft() != nullptr) postorderTraversal(*node.getLeft(), callback);
    if (node.getRight() != nullptr) postorderTraversal(*node.getRight(), callback);
    callback(node);
}

template<typename T>
void TreeTraversal<T>::inorderTraversal(BinaryTreeNode<T>& node, cb callback) {
    if (node.getLeft() != nullptr) inorderTraversal(*node.getLeft(), callback);
    callback(node);
    if (node.getRight() != nullptr) inorderTraversal(*node.getRight(), callback);
}
}

template class ds::TreeTraversal<int>;
template class ds::TreeTraversal<std::string>;
