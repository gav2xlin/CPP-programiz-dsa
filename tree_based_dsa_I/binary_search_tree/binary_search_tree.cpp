#include "binary_search_tree.h"
#include <stdexcept>
#include <sstream>
#include <utility>

namespace ds {
// private node methods
template<typename T>
void BinarySearchTree<T>::Node::inorder(std::ostream& os) {
    if (left != nullptr) left->inorder(os);
    os << key << ' ';
    if (right != nullptr) right->inorder(os);
}

template<typename T>
void BinarySearchTree<T>::Node::preorder(std::ostream& os) {
    os << key << ' ';
    if (left != nullptr) left->preorder(os);
    if (right != nullptr) right->preorder(os);
}

template<typename T>
void BinarySearchTree<T>::Node::postorder(std::ostream& os) {
    if (left != nullptr) left->postorder(os);
    if (right != nullptr) right->postorder(os);
    os << key << ' ';
}

// private methods
template<typename T>
void BinarySearchTree<T>::deleteAll(Node *node) {
    if (node != nullptr) {
        if (node->left != nullptr) {
            deleteAll(node->left);
            node->left = nullptr;
        }

        if (node->right != nullptr) {
            deleteAll(node->right);
            node->right = nullptr;
        }

        delete node;
    }
}

template<typename T>
void BinarySearchTree<T>::copyNodes(Node* dst, const Node* src) {
    dst->key = src->key;
    dst->left = dst->right = nullptr;

    if (src->left) {
        dst->left = new Node{.key = src->left->key};
        copyNodes(dst->left, src->left);
    }

    if (src->right) {
        dst->right = new Node{.key = src->right->key};
        copyNodes(dst->right, src->right);
    }
}

template<typename T>
typename BinarySearchTree<T>::Node* BinarySearchTree<T>::_insertNode(Node* node, T key) {
    if (node == nullptr) {
        return new Node{.key = key};
    }

    if (key < node->key) {
        node->left = _insertNode(node->left, key);
    } else if (key > node->key) {
        node->right = _insertNode(node->right, key);
    } else {
        std::stringstream ss("Duplicated key ");
        ss << key;
        throw std::invalid_argument(ss.str());
    }

    return node;
}

template<typename T>
bool BinarySearchTree<T>::_hasNode(Node* node, T key) {
    if (node != nullptr) {
        if (node->key == key) return true;

        if (key < node->key) {
            return _hasNode(node->left, key);
        } else if (key > node->key) {
            return _hasNode(node->right, key);
        }
    }
    return false;
}

template<typename T>
typename BinarySearchTree<T>::Node* BinarySearchTree<T>::findLeftMostNode(Node *node) {
    Node *current = node;

    while (current && current->left != NULL) {
        current = current->left;
    }

    return current;
}

template<typename T>
typename BinarySearchTree<T>::Node* BinarySearchTree<T>::_deleteNode(Node* node, T key) {
    if (node == nullptr) return node;

    if (key < node->key) {
        node->left = _deleteNode(node->left, key);
    } else if (key > node->key) {
        node->right = _deleteNode(node->right, key);
    } else if (key == node->key) {
        // if the node is with only one child or no child
        if (node->left == nullptr) {
            Node *temp = node->right;
            delete node;
            return temp;
        } else if (node->right == nullptr) {
            Node *temp = node->left;
            delete node;
            return temp;
        }

        // if the node has two children
        Node* temp = findLeftMostNode(node->right);

        // place the inorder successor in position of the node to be deleted
        node->key = temp->key;

        // delete the inorder successor
        node->right = _deleteNode(node->right, temp->key);
    } else {
        std::stringstream ss("Nonexistent key ");
        ss << key;
        throw std::invalid_argument(ss.str());
    }

    return node;
}

// public methods
template<typename T>
BinarySearchTree<T>::~BinarySearchTree()
{
    if (root != nullptr) {
        deleteAll(root->left);
        deleteAll(root->right);
        delete root;
    }
}

template<typename T>
BinarySearchTree<T>::BinarySearchTree(const BinarySearchTree &other) {
    if (other.root) {
        root = new Node{};
        copyNodes(root, other.root);
    }
}

template<typename T>
BinarySearchTree<T>& BinarySearchTree<T>::operator=(const BinarySearchTree& other) {
    if (this != &other) {
        if (root != nullptr) {
            deleteAll(root->left);
            deleteAll(root->right);
            root = nullptr;
        }

        if (other.root) {
            root = new Node{};
            copyNodes(root, other.root);
        }
    }
    return *this;
}

template<typename T>
BinarySearchTree<T>::BinarySearchTree(BinarySearchTree &&other) noexcept {
    //root = other.root;
    //other.root = nullptr;
    *this = std::move(other);
}

template<typename T>
BinarySearchTree<T>& BinarySearchTree<T>::operator=(BinarySearchTree&& other) noexcept {
    if (this != &other) {
        if (root != nullptr) {
            deleteAll(root->left);
            deleteAll(root->right);
            root = nullptr;
        }

        root = other.root;
        other.root = nullptr;
    }
    return *this;
}

template<typename T>
void BinarySearchTree<T>::inorder(std::ostream& os) {
    if (root != nullptr) {
        root->inorder(os);
    }
}

template<typename T>
void BinarySearchTree<T>::preorder(std::ostream& os) {
    if (root != nullptr) {
        root->preorder(os);
    }
}

template<typename T>
void BinarySearchTree<T>::postorder(std::ostream& os) {
    if (root != nullptr) {
        root->postorder(os);
    }
}

template<typename T>
void BinarySearchTree<T>::insertNode(T key) {
    root = _insertNode(root, key);
}

template<typename T>
bool BinarySearchTree<T>::hasNode(T key) {
    return _hasNode(root, key);
}

template<typename T>
void BinarySearchTree<T>::deleteNode(T key) {
    root = _deleteNode(root, key);
}
}

template class ds::BinarySearchTree<int>;
