cmake_minimum_required(VERSION 3.5)

project(binary_search_tree LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include(FetchContent)
FetchContent_Declare(
  googletest
  GIT_REPOSITORY https://github.com/google/googletest.git
  GIT_TAG        release-1.12.1
)
# For Windows: Prevent overriding the parent project's compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

set(BINARY_SEARCH_TREE_SOURCES binary_search_tree.cpp)
set(BINARY_SEARCH_TREE_HEADERS binary_search_tree.h)

add_executable(binary_search_tree_test ${BINARY_SEARCH_TREE_SOURCES} ${BINARY_SEARCH_TREE_HEADERS} test.cpp)
target_link_libraries(binary_search_tree_test gtest_main gmock)
add_test(NAME binary_search_tree_test COMMAND binary_search_tree_test)

add_executable(binary_search_tree ${BINARY_SEARCH_TREE_SOURCES} ${BINARY_SEARCH_TREE_HEADERS} main.cpp)
target_link_libraries(binary_search_tree gtest gmock)
