#include "binary_search_tree.h"
#include <iostream>
#include <utility> // std::move

using namespace std;
using namespace ds;

int main() {
    BinarySearchTree<int> tree;
    tree.insertNode(8);
    tree.insertNode(3);
    tree.insertNode(1);
    tree.insertNode(6);
    tree.insertNode(7);
    tree.insertNode(10);
    tree.insertNode(14);
    tree.insertNode(4);

    cout << boolalpha << "has 10: " << tree.hasNode(10) << endl;
    cout << "has 0: " << tree.hasNode(0) << endl;

    cout << "Inorder traversal: ";
    tree.inorder(cout);

    cout << "\nPreorder traversal: ";
    tree.preorder(cout);

    cout << "\nPostorder traversal: ";
    tree.postorder(cout);

    cout << "\nAfter deleting 10\n";
    tree.deleteNode(10);

    cout << "has 10: " << tree.hasNode(10) << endl;

    cout << "Inorder traversal: ";
    tree.inorder(cout);

    cout << endl;

    return 0;
}
