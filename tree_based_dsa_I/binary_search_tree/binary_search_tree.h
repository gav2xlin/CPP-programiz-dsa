#ifndef BINARY_SEARCH_TREE_H
#define BINARY_SEARCH_TREE_H

#include <iostream>

namespace ds {
template<typename T>
class BinarySearchTree
{
private:
    struct Node {
        int key;
        Node *left{nullptr}, *right{nullptr};

        void inorder(std::ostream& os);
        void preorder(std::ostream& os);
        void postorder(std::ostream& os);
    };

    Node* root{nullptr};

    void deleteAll(Node *node);
    void copyNodes(Node* dst, const Node* src);

    Node* _insertNode(Node* node, T key);
    bool _hasNode(Node* node, T key);

    Node* findLeftMostNode(Node *node);
    Node* _deleteNode(Node* node, T key);
public:
    BinarySearchTree() = default;
    ~BinarySearchTree();

    BinarySearchTree(const BinarySearchTree &other); // copy constructor
    BinarySearchTree& operator=(const BinarySearchTree& other); // copy assignment operator

    BinarySearchTree(BinarySearchTree&& other) noexcept; // move constructor
    BinarySearchTree& operator=(BinarySearchTree&& other) noexcept; // move assignment operator

    void inorder(std::ostream& os);
    void preorder(std::ostream& os);
    void postorder(std::ostream& os);

    void insertNode(T key);
    bool hasNode(T key);
    void deleteNode(T key);
};
}

#endif // BINARY_SEARCH_TREE_H
