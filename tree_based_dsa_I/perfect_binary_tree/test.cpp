#include "binary_tree_node.h"
#include "binary_tree_aux.h"
#include "gtest/gtest.h"
#include "gmock/gmock-matchers.h"

namespace ds {
class PerfectBinaryTreeTest : public testing::Test {
protected:
    BinaryTreeNode<int>* root{nullptr};

    virtual void TearDown() {
        delete root;
    }
};

TEST_F(PerfectBinaryTreeTest, IsPerfectBinaryTree) {
    root = new BinaryTreeNode<int>(1);
    root->addLeft(2);
    root->addRight(3);
    root->getLeft()->addLeft(4);
    root->getLeft()->addRight(5);
    root->getRight()->addLeft(6);
    root->getRight()->addRight(7);

    ASSERT_TRUE(is_perfect_binary_tree(root));
}

TEST_F(PerfectBinaryTreeTest, NotIsPerfectBinaryTree) {
    root = new BinaryTreeNode<int>(1);
    root->addLeft(2);
    root->addRight(3);
    root->getLeft()->addLeft(4);

    ASSERT_FALSE(is_perfect_binary_tree(root));
}
};
