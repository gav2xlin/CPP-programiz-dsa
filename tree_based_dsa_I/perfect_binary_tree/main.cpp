#include "binary_tree_node.h"
#include "binary_tree_aux.h"
#include <iostream>

using namespace std;
using namespace ds;

int main() {
    BinaryTreeNode<int> root(1);
    root.addLeft(2);
    root.addRight(3);
    root.getLeft()->addLeft(4);

    if (is_perfect_binary_tree(&root)) {
        cout << "The tree is a perfect binary tree" << endl;
    } else {
        cout << "The tree is not a perfect binary tree" << endl;
    }

    BinaryTreeNode<int> n(1);
    n.addLeft(2);
    n.addRight(3);
    n.getLeft()->addLeft(4);
    n.getLeft()->addRight(5);
    n.getRight()->addLeft(6);
    n.getRight()->addRight(7);

    if (is_perfect_binary_tree(&n)) {
        cout << "The tree is a perfect binary tree" << endl;
    } else {
        cout << "The tree is not a perfect binary tree" << endl;
    }

    return 0;
}
