#ifndef BINARY_TREE_AUX_H
#define BINARY_TREE_AUX_H

#include "binary_tree_node.h"

namespace ds {
template<typename T>
int is_perfect_binary_tree(BinaryTreeNode<T>* node, int level=0) {
    if (!node) return level;

    if (!node->getLeft() && !node->getRight()) return level + 1;

    if (node->getLeft() && node->getRight()) {
        int left_level = is_perfect_binary_tree(node->getLeft(), level + 1);
        int right_level = is_perfect_binary_tree(node->getRight(), level + 1);
        return left_level == right_level ? left_level : 0;
    }

    return 0;
}
}

#endif // BINARY_TREE_AUX_H
