#ifndef BINARY_TREE_AUX_H
#define BINARY_TREE_AUX_H

#include "binary_tree_node.h"

namespace ds {
template<typename T>
bool is_full_binary_tree(BinaryTreeNode<T>* node) {
    if (!node) return true;

    if (!node->getLeft() && !node->getRight()) return true;

    if (node->getLeft() && node->getRight()) {
        return is_full_binary_tree(node->getLeft()) && is_full_binary_tree(node->getRight());
    }

    return false;
}
}

#endif // BINARY_TREE_AUX_H
