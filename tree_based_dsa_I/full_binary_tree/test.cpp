#include "binary_tree_node.h"
#include "binary_tree_aux.h"
#include "gtest/gtest.h"
#include "gmock/gmock-matchers.h"

namespace ds {
class FullBinaryTreeTest : public testing::Test {
protected:
    BinaryTreeNode<int>* root{nullptr};

    virtual void TearDown() {
        delete root;
    }
};

TEST_F(FullBinaryTreeTest, IsFullBinaryTree) {
    root = new BinaryTreeNode<int>(1);
    root->addLeft(2);
    root->addRight(3);
    root->getLeft()->addLeft(4);
    root->getLeft()->addRight(5);
    root->getLeft()->getRight()->addLeft(6);
    root->getLeft()->getRight()->addRight(7);

    ASSERT_TRUE(is_full_binary_tree(root));
}

TEST_F(FullBinaryTreeTest, NotIsFullBinaryTree) {
    root = new BinaryTreeNode<int>(1);
    root->addLeft(2);
    root->addRight(3);
    root->getLeft()->addLeft(4);

    ASSERT_FALSE(is_full_binary_tree(root));
}
};
