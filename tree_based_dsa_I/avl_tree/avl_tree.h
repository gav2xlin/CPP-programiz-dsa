#ifndef AVL_TREE_H
#define AVL_TREE_H

#include <string>

namespace ds {
template<typename T>
class AVLTree
{
private:
    struct Node {
        int key, height{1};
        Node *left{nullptr}, *right{nullptr};
    };

    Node* root{nullptr};

    void deleteAll(Node *node);
    static void printTree(std::ostream& os, Node *node, std::string indent, bool rln);

    Node* _insertNode(Node* node, T key);
    bool _hasNode(Node* node, T key);

    Node* findLeftMostNode(Node *node);
    Node* _deleteNode(Node* node, T key);

    int height(Node *node);
    int balanceFactor(Node *node);

    Node *leftRotate(Node *x);
    Node *rightRotate(Node *y);
public:
    AVLTree() = default;
    ~AVLTree();

    AVLTree(const AVLTree &other) = delete; // copy constructor
    AVLTree& operator=(const AVLTree& other) = delete; // copy assignment operator

    AVLTree(AVLTree&& other) = delete; // move constructor
    AVLTree& operator=(AVLTree&& other) = delete; // move assignment operator

    void insertNode(T key);
    bool hasNode(T key);
    void deleteNode(T key);

    friend std::ostream& operator<<(std::ostream& os, const AVLTree<T>& tree) {
        AVLTree<T>::printTree(os, tree.root, "", true);
        return os;
    }
};
}

#endif // AVL_TREE_H
