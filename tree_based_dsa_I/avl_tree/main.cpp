#include "avl_tree.h"
#include <iostream>

using namespace std;
using namespace ds;

int main()
{
    AVLTree<int> tree;
    tree.insertNode(33);
    tree.insertNode(13);
    tree.insertNode(53);
    tree.insertNode(9);
    tree.insertNode(21);
    tree.insertNode(61);
    tree.insertNode(8);
    tree.insertNode(11);

    cout << tree << endl;

    tree.deleteNode(13);

    cout << "After deleting: 13 " << endl;
    cout << tree << endl;

    return 0;
}
