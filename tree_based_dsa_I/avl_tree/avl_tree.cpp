#include "avl_tree.h"
#include <sstream>
#include <stdexcept>
#include <algorithm>

namespace ds {
// private methods
template<typename T>
void AVLTree<T>::deleteAll(Node *node) {
    if (node != nullptr) {
        if (node->left != nullptr) {
            deleteAll(node->left);
            node->left = nullptr;
        }

        if (node->right != nullptr) {
            deleteAll(node->right);
            node->right = nullptr;
        }

        delete node;
    }
}

template<typename T>
void AVLTree<T>::printTree(std::ostream& os, Node *node, std::string indent, bool rln) {
    if (node != nullptr) {
        os << indent;
        if (rln) {
            os << "R----";
            indent += "   ";
        } else {
            os << "L----";
            indent += "|  ";
        }
        os << node->key << '\n';
        printTree(os, node->left, indent, false);
        printTree(os, node->right, indent, true);
    }
}

template<typename T>
bool AVLTree<T>::_hasNode(Node* node, T key) {
    if (node != nullptr) {
        if (node->key == key) return true;

        if (key < node->key) {
            return _hasNode(node->left, key);
        } else if (key > node->key) {
            return _hasNode(node->right, key);
        }
    }
    return false;
}

template<typename T>
typename AVLTree<T>::Node* AVLTree<T>::_insertNode(Node* node, T key) {
    // binary search tree
    if (node == nullptr) {
        return new Node{.key = key}; // height = 1
    }

    if (key < node->key) {
        node->left = _insertNode(node->left, key);
    } else if (key > node->key) {
        node->right = _insertNode(node->right, key);
    } else {
        std::stringstream ss("Duplicated key ");
        ss << key;
        throw std::invalid_argument(ss.str());
    }

    // update the balance factor of each node and balance the tree
    node->height = std::max(height(node->left), height(node->right)) + 1;

    int bf = balanceFactor(node);

    if (bf > 1) {
        if (key < node->left->key) {
            return rightRotate(node);
        } else if (key > node->left->key) {
            node->left = leftRotate(node->left);
            return rightRotate(node);
        }
    }

    if (bf < -1) {
        if (key > node->right->key) {
            return leftRotate(node);
         } else if (key < node->right->key) {
            node->right = rightRotate(node->right);
            return leftRotate(node);
        }
    }

    return node;
}

template<typename T>
typename AVLTree<T>::Node* AVLTree<T>::_deleteNode(Node* node, T key) {
    // binary search tree
    if (node == nullptr) return node;

    if (key < node->key) {
        node->left = _deleteNode(node->left, key);
    } else if (key > node->key) {
        node->right = _deleteNode(node->right, key);
    } else if (key == node->key) {
        if (node->left == nullptr || node->right == nullptr) { // if the node is with only one child or no child
            Node *temp = node->left ? node->left : node->right;
            delete node;
            node = temp;
        } else { // if the node has two children
            Node* temp = findLeftMostNode(node->right);

            // place the inorder successor in position of the node to be deleted
            node->key = temp->key;

            // delete the inorder successor
            node->right = _deleteNode(node->right, temp->key);
        }
    } else {
        std::stringstream ss("Nonexistent key ");
        ss << key;
        throw std::invalid_argument(ss.str());
    }

    // avl tree
    if (node == nullptr) return node;

    // update the balance factor of each node and balance the tree
    node->height = 1 + std::max(height(node->left), height(node->right)) + 1;

    int bf = balanceFactor(node);

    if (bf > 1) {
        if (balanceFactor(node->left) >= 0) {
            return rightRotate(node);
        } else {
            node->left = leftRotate(node->left);
            return rightRotate(node);
        }
    }

    if (bf < -1) {
        if (balanceFactor(node->right) <= 0) {
            return leftRotate(node);
        } else {
            node->right = rightRotate(node->right);
            return leftRotate(node);
        }
    }

    return node;
}

template<typename T>
typename AVLTree<T>::Node* AVLTree<T>::findLeftMostNode(Node *node) {
    Node *current = node;

    while (current && current->left != NULL) {
        current = current->left;
    }

    return current;
}

template<typename T>
typename AVLTree<T>::Node* AVLTree<T>::leftRotate(Node *x) {
    Node *y = x->right;
    Node *b = y->left;
    y->left = x;
    x->right = b;

    x->height = std::max(height(x->left), height(x->right)) + 1;
    y->height = std::max(height(y->left), height(y->right)) + 1;

    return y;
}

template<typename T>
typename AVLTree<T>::Node* AVLTree<T>::rightRotate(Node *y) {
    Node *x = y->left;
    Node *b = x->right;
    x->right = y;
    y->left = b;

    y->height = std::max(height(y->left), height(y->right)) + 1;
    x->height = std::max(height(x->left), height(x->right)) + 1;

    return x;
}

template<typename T>
inline int AVLTree<T>::height(Node *node) {
    return node != nullptr ? node->height : 0;
}

template<typename T>
inline int AVLTree<T>::balanceFactor(Node *node) {
    return node != nullptr ? height(node->left) - height(node->right) : 0;
}

// public methods
template<typename T>
AVLTree<T>::~AVLTree()
{
    if (root != nullptr) {
        deleteAll(root->left);
        deleteAll(root->right);

        delete root;
    }
}

template<typename T>
void AVLTree<T>::insertNode(T key) {
    root = _insertNode(root, key);
}

template<typename T>
bool AVLTree<T>::hasNode(T key) {
    return _hasNode(root, key);
}

template<typename T>
void AVLTree<T>::deleteNode(T key) {
    root = _deleteNode(root, key);
}
}

template class ds::AVLTree<int>;
