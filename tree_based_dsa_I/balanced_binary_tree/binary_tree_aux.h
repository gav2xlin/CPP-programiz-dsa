#ifndef BINARY_TREE_AUX_H
#define BINARY_TREE_AUX_H

#include "binary_tree_node.h"
#include <cstdlib>

namespace ds {
template<typename T>
bool is_balanced_binary_tree(BinaryTreeNode<T>* node, int& height) {
    if (node == nullptr) {
        height = 0;
        return true;
    }

    int lh = 0, rh = 0;

    bool lb = is_balanced_binary_tree(node->getLeft(), lh);
    bool rb = is_balanced_binary_tree(node->getRight(), rh);

    height = (lh > rh ? lh : rh) + 1;

    if (abs(lh - rh) >= 2) {
        return false;
    } else {
        return lb && rb;
    }
}
}

#endif // BINARY_TREE_AUX_H
