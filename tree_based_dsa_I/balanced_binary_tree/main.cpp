#include "binary_tree_node.h"
#include "binary_tree_aux.h"
#include <iostream>

using namespace std;
using namespace ds;

int main() {
    BinaryTreeNode<int> root(1);
    root.addLeft(2);
    root.addRight(3);
    root.getLeft()->addLeft(4);
    root.getLeft()->addRight(5);

    int height = 0;
    if (is_balanced_binary_tree(&root, height)) {
        cout << "The tree is balanced" << endl;
    } else {
        cout << "The tree is not balanced" << endl;
    }

    return 0;
}
