#include "binary_tree_node.h"
#include "binary_tree_aux.h"
#include "gtest/gtest.h"
#include "gmock/gmock-matchers.h"

namespace ds {
class BalancedBinaryTreeTest : public testing::Test {
protected:
    BinaryTreeNode<int>* root{nullptr};

    virtual void TearDown() {
        delete root;
    }
};

TEST_F(BalancedBinaryTreeTest, IsCompleteBinaryTree) {
    root = new BinaryTreeNode<int>(1);
    root->addLeft(2);
    root->addRight(3);
    root->getLeft()->addLeft(4);
    root->getLeft()->addRight(5);
    root->getRight()->addLeft(6);

    int height = 0;
    ASSERT_TRUE(is_balanced_binary_tree(root, height));
}

TEST_F(BalancedBinaryTreeTest, NotIsPerfectBinaryTree) {
    root = new BinaryTreeNode<int>(1);
    root->addLeft(2);
    root->addRight(3);
    root->getLeft()->addLeft(4);
    root->getLeft()->addRight(5);
    root->getLeft()->getRight()->addLeft(6);

    int height = 0;
    ASSERT_FALSE(is_balanced_binary_tree(root, height));
}
};
