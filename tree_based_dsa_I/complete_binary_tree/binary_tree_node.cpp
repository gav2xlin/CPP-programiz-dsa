#include "binary_tree_node.h"
#include <stdexcept>
#include <string>

namespace ds {
// private methods

template<typename T>
void BinaryTreeNode<T>::deleteAll(BinaryTreeNode *node) {
    if (node != nullptr) {
        if (node->left != nullptr) {
            deleteAll(node->left);
            node->left = nullptr;
        }

        if (node->right != nullptr) {
            deleteAll(node->right);
            node->right = nullptr;
        }

        delete node;
    }
}

template<typename T>
void BinaryTreeNode<T>::copyNodes(BinaryTreeNode* dst, const BinaryTreeNode* src) {
    dst->data = src->data;
    dst->left =  dst->right = nullptr;

    if (src->left) {
        dst->addLeft(src->left->data);
        copyNodes(dst->getLeft(), src->left);
    }

    if (src->right) {
        dst->addRight(src->right->data);
        copyNodes(dst->getRight(), src->right);
    }
}

// public methods

template<typename T>
BinaryTreeNode<T>::BinaryTreeNode(T value) {
    data = value;
    left = right = nullptr;
}

template<typename T>
BinaryTreeNode<T>::~BinaryTreeNode() {
    removeLeft();
    removeRight();
}

template<typename T>
T BinaryTreeNode<T>::getData() {
    return data;
}

template<typename T>
void BinaryTreeNode<T>::setData(T value) {
    data = value;
}

template<typename T>
BinaryTreeNode<T>* BinaryTreeNode<T>::getLeft() {
    return left;
}

template<typename T>
void BinaryTreeNode<T>::addLeft(T value) {
    if (left != nullptr) {
        throw std::logic_error("The left node has already been created");
    }
    if ((data >= value) || (right != nullptr && right->data <= value)) {
        throw std::invalid_argument("Binary Tree rule violation");
    }
    left = new BinaryTreeNode<T>(value);
}

template<typename T>
void BinaryTreeNode<T>::removeLeft() {
    if (left != nullptr) {
        deleteAll(left);
        left = nullptr;
    }
}

template<typename T>
BinaryTreeNode<T>* BinaryTreeNode<T>::getRight() {
    return right;
}

template<typename T>
void BinaryTreeNode<T>::addRight(T value) {
    if (right != nullptr) {
        throw std::logic_error("The right node has already been created");
    }
    if ((data >= value) || (left != nullptr && left->data >= value)) {
        throw std::invalid_argument("Binary Tree rule violation");
    }
    right = new BinaryTreeNode<T>(value);
}

template<typename T>
void BinaryTreeNode<T>::removeRight() {
    if (right != nullptr) {
        deleteAll(right);
        right = nullptr;
    }
}

template<typename T>
BinaryTreeNode<T>::BinaryTreeNode(const BinaryTreeNode &other) {
    copyNodes(this, &other);
}

template<typename T>
BinaryTreeNode<T>& BinaryTreeNode<T>::operator=(const BinaryTreeNode& other) {
    if (this != &other) {
        removeLeft();
        removeRight();

        copyNodes(this, &other);
    }
    return *this;
}
}

template class ds::BinaryTreeNode<int>;
template class ds::BinaryTreeNode<std::string>;
