#ifndef BINARY_TREE_AUX_H
#define BINARY_TREE_AUX_H

#include "binary_tree_node.h"

namespace ds {
template<typename T>
int count_num_nodes(BinaryTreeNode<T>* node) {
    return (node == nullptr) ? 0 : 1 + count_num_nodes(node->getLeft()) + count_num_nodes(node->getRight());
}

template<typename T>
bool is_complete_binary_tree(BinaryTreeNode<T>* node, int index, int number_nodes) {
    if (!node) return true;

    if (index >= number_nodes) return false;

    return is_complete_binary_tree(node->getLeft(), 2 * index + 1, number_nodes) &&
           is_complete_binary_tree(node->getRight(), 2 * index + 2, number_nodes); // heap data structure indexes: i, 2*i + 1, 2*i + 2
}
}

#endif // BINARY_TREE_AUX_H
