#include "binary_tree_node.h"
#include "binary_tree_aux.h"
#include "gtest/gtest.h"
#include "gmock/gmock-matchers.h"

namespace ds {
class CompleteBinaryTreeTest : public testing::Test {
protected:
    BinaryTreeNode<int>* root{nullptr};

    virtual void TearDown() {
        delete root;
    }
};

TEST_F(CompleteBinaryTreeTest, IsCompleteBinaryTree) {
    root = new BinaryTreeNode<int>(1);
    root->addLeft(2);
    root->addRight(3);
    root->getLeft()->addLeft(4);
    root->getLeft()->addRight(5);
    root->getRight()->addLeft(6);

    int node_count = count_num_nodes(root);

    ASSERT_TRUE(is_complete_binary_tree(root, 0, node_count));
}

TEST_F(CompleteBinaryTreeTest, NotIsPerfectBinaryTree) {
    root = new BinaryTreeNode<int>(1);
    root->addLeft(2);
    root->addRight(3);
    root->getLeft()->addLeft(4);
    root->getLeft()->addRight(5);
    root->getLeft()->getRight()->addLeft(6);

    int node_count = count_num_nodes(root);

    ASSERT_FALSE(is_complete_binary_tree(root, 0, node_count));
}
};
