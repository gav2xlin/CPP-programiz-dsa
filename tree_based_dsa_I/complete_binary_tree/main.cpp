#include "binary_tree_node.h"
#include "binary_tree_aux.h"
#include <iostream>

using namespace std;
using namespace ds;

int main() {
    BinaryTreeNode<int> root(1);
    root.addLeft(2);
    root.addRight(3);
    root.getLeft()->addLeft(4);
    root.getLeft()->addRight(5);
    root.getRight()->addLeft(6);

    int node_count = count_num_nodes(&root);

    if (is_complete_binary_tree(&root, 0, node_count)) {
        cout << "The tree is a complete binary tree" << endl;
    } else {
        cout << "The tree is not a complete binary tree" << endl;
    }

    return 0;
}
