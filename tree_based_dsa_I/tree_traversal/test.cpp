#include "tree_node.h"
#include "tree_traversal.h"
#include "gtest/gtest.h"
#include "gmock/gmock-matchers.h"
#include <vector>

namespace ds {
class TreeTraversalTest : public testing::Test {
protected:
    TreeNode<int>* root;
    TreeTraversal<int> traversal;

    virtual void SetUp() {
        root = new TreeNode<int>(1);
        root->addLeft(12);
        root->addRight(9);
        root->getLeft()->addLeft(5);
        root->getLeft()->addRight(6);
    }

    virtual void TearDown() {
        delete root;
    }
};

TEST_F(TreeTraversalTest, PreorderTraversal) {
    std::vector<int> res;
    auto cb = [&res](auto& val) {
        res.push_back(val.getData());
    };
    traversal.preorderTraversal(*root, cb);

    using ::testing::ElementsAreArray;

    EXPECT_THAT(res, ElementsAreArray({1, 12, 5, 6, 9}));
}

TEST_F(TreeTraversalTest, PostorderTraversal) {
    std::vector<int> res;
    auto cb = [&res](auto& val) {
        res.push_back(val.getData());
    };
    traversal.postorderTraversal(*root, cb);

    using ::testing::ElementsAreArray;

    EXPECT_THAT(res, ElementsAreArray({5, 6, 12, 9, 1}));
}

TEST_F(TreeTraversalTest, InorderTraversal) {
    std::vector<int> res;
    auto cb = [&res](auto& val) {
        res.push_back(val.getData());
    };
    traversal.inorderTraversal(*root, cb);

    using ::testing::ElementsAreArray;

    EXPECT_THAT(res, ElementsAreArray({5, 12, 6, 1, 9}));
}
};
