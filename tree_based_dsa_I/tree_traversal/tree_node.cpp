#include "tree_node.h"
#include <stdexcept>
#include <string>

namespace ds {
// private methods

template<typename T>
void TreeNode<T>::deleteAll(TreeNode *node) {
    if (node != nullptr) {
        if (node->left != nullptr) {
            deleteAll(node->left);
            node->left = nullptr;
        }

        if (node->right != nullptr) {
            deleteAll(node->right);
            node->right = nullptr;
        }

        delete node;
    }
}

template<typename T>
void TreeNode<T>::copyNodes(TreeNode* dst, const TreeNode* src) {
    dst->data = src->data;
    dst->left = dst->right = nullptr;

    if (src->left) {
        dst->addLeft(src->left->data);
        copyNodes(dst->getLeft(), src->left);
    }

    if (src->right) {
        dst->addRight(src->right->data);
        copyNodes(dst->getRight(), src->right);
    }
}

// public methods

template<typename T>
TreeNode<T>::TreeNode(T value) {
    data = value;
    left = right = nullptr;
}

template<typename T>
TreeNode<T>::~TreeNode() {
    removeLeft();
    removeRight();
}

template<typename T>
T TreeNode<T>::getData() {
    return data;
}

template<typename T>
void TreeNode<T>::setData(T value) {
    data = value;
}

template<typename T>
TreeNode<T>* TreeNode<T>::getLeft() {
    return left;
}

template<typename T>
void TreeNode<T>::addLeft(T value) {
    if (left != nullptr) {
        throw std::logic_error("The left node has already been created");
    }
    left = new TreeNode<T>(value);
}

template<typename T>
void TreeNode<T>::removeLeft() {
    if (left != nullptr) {
        deleteAll(left);
        left = nullptr;
    }
}

template<typename T>
TreeNode<T>* TreeNode<T>::getRight() {
    return right;
}

template<typename T>
void TreeNode<T>::addRight(T value) {
    if (right != nullptr) {
        throw std::logic_error("The right node has already been created");
    }
    right = new TreeNode<T>(value);
}

template<typename T>
void TreeNode<T>::removeRight() {
    if (right != nullptr) {
        deleteAll(right);
        right = nullptr;
    }
}

template<typename T>
TreeNode<T>::TreeNode(const TreeNode &other) {
    copyNodes(this, &other);
}

template<typename T>
TreeNode<T>& TreeNode<T>::operator=(const TreeNode& other) {
    if (this != &other) {
        removeLeft();
        removeRight();

        copyNodes(this, &other);
    }
    return *this;
}
}

template class ds::TreeNode<int>;
template class ds::TreeNode<std::string>;
