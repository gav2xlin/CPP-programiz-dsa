#ifndef TREETRAVERSAL_H
#define TREETRAVERSAL_H

#include "tree_node.h"
//#include "gtest/gtest.h"
#include <functional>

namespace ds {
template<typename T>
class TreeTraversal {
public:
    using cb = std::function<void(TreeNode<T>& node)>;

    void preorderTraversal(TreeNode<T>& node, cb callback);
    void postorderTraversal(TreeNode<T>& node, cb callback);
    void inorderTraversal(TreeNode<T>& node, cb callback);
};
}

#endif // TREETRAVERSAL_H
