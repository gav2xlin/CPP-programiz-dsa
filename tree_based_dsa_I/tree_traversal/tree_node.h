#ifndef TREE_NODE_H
#define TREE_NODE_H

#include "gtest/gtest.h"
#include <iostream>
//#include <utility> // std::move

namespace ds {
template <typename T>
class TreeNode {
private:
    T data;
    TreeNode *left, *right;

    void deleteAll(TreeNode *node);
    void copyNodes(TreeNode* dst, const TreeNode* src);
public:
    FRIEND_TEST(TreeTraversalTest, PreorderTraversal);
    FRIEND_TEST(TreeTraversalTest, PostorderTraversal);
    FRIEND_TEST(TreeTraversalTest, InorderTraversal);

    TreeNode(T value);
    ~TreeNode();

    T getData();
    void setData(T value);

    TreeNode* getLeft();
    void addLeft(T value);
    void removeLeft();

    TreeNode* getRight();
    void addRight(T value);
    void removeRight();

    TreeNode(const TreeNode &other); // copy constructor
    TreeNode& operator=(const TreeNode& other); // copy assignment operator

    //TreeNode(TreeNode&& other) noexcept; // move constructor
    //TreeNode& operator=(TreeNode&& other) noexcept; // move assignment operator

    // https://en.cppreference.com/w/cpp/language/friend

    friend std::ostream& operator<<(std::ostream& os, const TreeNode& node) {
        os << node.data;
        return os;
    }
};
}

#endif // TREE_NODE_H
