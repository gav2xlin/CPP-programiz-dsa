#include "tree_node.h"
#include "tree_traversal.h"
#include <iostream>

using namespace std;
using namespace ds;

int main() {
    TreeNode<int> root(1);
    root.addLeft(12);
    root.addRight(9);
    root.getLeft()->addLeft(5);
    root.getLeft()->addRight(6);

    // auto cb = [](auto val) { // using a copy constructor
    auto cb = [](auto& val) {
        cout << val << ' ';
    };

    TreeTraversal<int> traversal;
    cout << "Inorder traversal ";
    traversal.inorderTraversal(root, cb);

    cout << "\nPreorder traversal ";
    traversal.preorderTraversal(root, cb);

    cout << "\nPostorder traversal ";
    traversal.postorderTraversal(root, cb);

    cout << endl;

    // copy constructor

    TreeNode<int> nc(root);

    cout << "\nCopy constructor ";

    traversal.inorderTraversal(nc, cb);

    cout << endl;

    // copy assignment operator

    TreeNode<int> n(0);

    cout << "\nBefore copying ";
    traversal.inorderTraversal(n, cb);

    n = root;

    cout << "\nAfter copying ";
    traversal.inorderTraversal(n, cb);

    cout << endl;

    return 0;
}
