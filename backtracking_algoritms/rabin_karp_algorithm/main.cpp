#include <iostream>
#include <cstring>

using namespace std;

#define d 10

void rabinKarp(char pattern[], char text[], int q) {
    int m = strlen(pattern);
    int n = strlen(text);

    int h = 1;
    for (int i = 0; i < m - 1; ++i) {
        h = (h * d) % q;
    }

    // calculate hash value for pattern and text
    int p = 0, t = 0;
    for (int i = 0; i < m; i++) {
        p = (d * p + pattern[i]) % q;
        t = (d * t + text[i]) % q;
    }

    // find the match
    for (int i = 0; i <= n - m; i++) {
        if (p == t) {
            int j;
            for (j = 0; j < m; j++) {
                if (text[i + j] != pattern[j])
                    break;
            }

            if (j == m) {
                cout << "Pattern is found at position: " << i + 1 << endl;
            }
        }

        if (i < n - m) {
            t = (d * (t - text[i] * h) + text[i + m]) % q;

            if (t < 0) {
                t = (t + q);
            }
        }
    }
}

int main() {
    char text[] = "ABCCDDAEFG";
    char pattern[] = "CDD";
    int q = 13;

    rabinKarp(pattern, text, q);

    return 0;
}
