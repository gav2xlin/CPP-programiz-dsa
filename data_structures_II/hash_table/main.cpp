#include "hash_table.h"
#include <iostream>

using namespace std;
using namespace ds;

int main (int argc, char *argv[]) {
    HashTable<int> hti(6);
    HashTable<string> hts(6);

    hti.insertItem(1, 1);
    hti.insertItem(2, 2);
    hti.insertItem(22, 4);
    std::cout << "has item:" << std::boolalpha << hti.hasItem(22) << std::endl;
    std::cout << hti << std::endl;

    hti.deleteItem(22);
    std::cout << "has item:" << std::boolalpha << hti.hasItem(22) << std::endl;
    std::cout << hti << std::endl;

    hts.insertItem(1, "a");
    hts.insertItem(2, "b");
    hts.insertItem(22, "c");
    std::cout << "has item: " << hts.hasItem(22) << std::endl;
    std::cout << hts << std::endl;

    hts.deleteItem(22);
    std::cout << "has item: " << hts.hasItem(22) << std::endl;
    std::cout << hts << std::endl;

    hti.getItem(1) = 3;
    std::cout << hti << std::endl;

    return 0;
}
