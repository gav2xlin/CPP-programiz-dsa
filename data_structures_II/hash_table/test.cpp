#include "hash_table.h"
#include "hash_table.cpp"
#include "gtest/gtest.h"
#include "gmock/gmock-matchers.h"
#include <vector>

namespace ds {
class HashTableTest : public testing::Test {
};

TEST_F(HashTableTest, NumberDeleteItem) {
    HashTable<int> ht(6);

    ht.insertItem(1, 1);
    ht.insertItem(2, 2);
    ht.insertItem(22, 4);

    EXPECT_TRUE(ht.deleteItem(22));
    EXPECT_FALSE(ht.deleteItem(22));
}

TEST_F(HashTableTest, StringDeleteItem) {
    HashTable<std::string> ht(6);

    ht.insertItem(1, "a");
    ht.insertItem(2, "b");
    ht.insertItem(22, "c");

    EXPECT_TRUE(ht.deleteItem(22));
    EXPECT_FALSE(ht.deleteItem(22));
}

TEST_F(HashTableTest, HasItem) {
    HashTable<int> ht(6);

    ht.insertItem(1, 1);
    ht.insertItem(2, 2);

    EXPECT_TRUE(ht.hasItem(2));
    EXPECT_FALSE(ht.hasItem(3));
}

TEST_F(HashTableTest, GetNumberItem) {
    HashTable<int> ht(6);

    ht.insertItem(1, 1);
    ht.insertItem(2, 2);

    EXPECT_EQ(1, ht.getItem(1));
    EXPECT_EQ(2, ht.getItem(2));
}

TEST_F(HashTableTest, GetStringItem) {
    HashTable<std::string> ht(6);

    ht.insertItem(1, "a");
    ht.insertItem(2, "b");

    EXPECT_EQ("a", ht.getItem(1));
    EXPECT_EQ("b", ht.getItem(2));
}

TEST_F(HashTableTest, NoItem) {
    HashTable<int> ht(6);

    ht.insertItem(1, 1);
    ht.insertItem(2, 2);

    EXPECT_EQ(1, ht.getItem(1));
    EXPECT_THROW(ht.getItem(10), std::out_of_range);
}

TEST_F(HashTableTest, DuplicatedKey) {
    HashTable<int> ht(6);

    ht.insertItem(1, 1);

    EXPECT_THROW(ht.insertItem(1, 1), std::invalid_argument);
}
}
