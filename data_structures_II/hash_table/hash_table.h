#ifndef HASH_TABLE_H
#define HASH_TABLE_H

#include "gtest/gtest.h"
#include <list>
#include <iostream>

namespace ds {
template<typename T>
class HashTable {
private:
    bool checkPrime(int n);
    int getPrime(int n);
    int hashFunction(int key);

    struct Pair {
        int key;
        T data;
    };

    friend std::ostream& operator<<(std::ostream& os, const HashTable<T>::Pair& pair) {
        os << "{" << pair.key << "->" << pair.data << "}";
        return os;
    }

    int capacity;
    std::list<Pair> *table;
public:
    HashTable(int c);
    ~HashTable();

    void insertItem(int key, T data);
    bool deleteItem(int key);
    bool hasItem(int key);
    T& getItem(int key);
    const T& getItem(int key) const;

    // https://en.cppreference.com/w/cpp/language/friend

    friend std::ostream& operator<<(std::ostream& os, const HashTable<T>& ht) {
        for (int i = 0; i < ht.capacity; ++i)
        {
            os << "[" << i << "]";
            for (auto &x : ht.table[i]) {
                std::cout << " --> " << x;
            }
            os << std::endl;
        }
        return os;
    }
};
}

#endif // HASH_TABLE_H
