#include "hash_table.h"
#include <stdexcept>
#include <sstream>
#include <string>
#include <algorithm>

namespace ds {
template<typename T>
HashTable<T>::HashTable(int c) {
    int size = getPrime(c);
    capacity = size;
    table = new std::list<HashTable<T>::Pair>[size];
}

template<typename T>
HashTable<T>::~HashTable() {
    delete[] table;
}

// https://en.wikipedia.org/wiki/Primality_test

template<typename T>
bool HashTable<T>::checkPrime(int n)
{
    if (n == 1 || n == 0)
    {
        return false;
    }
    for (int i = 2; i < n / 2; ++i)
    {
        if (n % i == 0)
        {
            return false;
        }
    }
    return true;
}

template<typename T>
int HashTable<T>::getPrime(int n)
{
    if (n % 2 == 0)
    {
        ++n;
    }
    while (!checkPrime(n))
    {
        n += 2;
    }
    return n;
}

template<typename T>
int HashTable<T>::hashFunction(int key)
{
    return (key % capacity);
}

template<typename T>
void HashTable<T>::insertItem(int key, T data)
{
    if (hasItem(key)) {
        std::ostringstream ss("Duplicated key: ");
        ss << key;
        throw std::invalid_argument(ss.str());
    }

    int index = hashFunction(key);
    table[index].push_back(Pair{key, data});
}

template<typename T>
bool HashTable<T>::deleteItem(int key)
{
    int index = hashFunction(key);

    auto it = std::find_if(table[index].begin(), table[index].end(), [key](const auto& item) {
        return item.key == key;
    });

    if (it != table[index].end()) {
        table[index].erase(it);
        return true;
    }

    return false;
}

template<typename T>
bool HashTable<T>::hasItem(int key)
{
    int index = hashFunction(key);

    auto it = std::find_if(table[index].begin(), table[index].end(), [key](const auto& item) {
        return item.key == key;
    });

    if (it != table[index].end()) {
        return true;
    }

    return false;
}

template<typename T>
T& HashTable<T>::getItem(int key)
{
    int index = hashFunction(key);

    auto it = std::find_if(table[index].begin(), table[index].end(), [key](const auto& item) {
        return item.key == key;
    });

    if (it != table[index].end()) {
        return it->data;
    }

    std::ostringstream ss("The item does not exist: ");
    ss << key;
    throw std::out_of_range(ss.str());
}

template<typename T>
const T& HashTable<T>::getItem(int key) const
{
    return getItem(key);
}
}

template class ds::HashTable<int>;
template class ds::HashTable<std::string>;
