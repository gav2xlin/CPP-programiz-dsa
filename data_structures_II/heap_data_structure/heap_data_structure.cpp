#include "heap_data_structure.h"
#include <stdexcept>

namespace ds {
// private methods
template<typename T>
void HeapDataStructure<T>::swap(T& a, T& b) {
    T temp = b;
    b = a;
    a = temp;
}

template<typename T>
void HeapDataStructure<T>::heapify(std::vector<T> &hT, int i) {
    int size = hT.size();

    int largest = i;
    int l = 2 * i + 1;
    int r = 2 * i + 2;

    if (l < size && hT[l] > hT[largest]) largest = l;
    if (r < size && hT[r] > hT[largest]) largest = r;

    if (largest != i) {
        swap(hT[i], hT[largest]);
        heapify(hT, largest);
    }
}

template<typename T>
void HeapDataStructure<T>::sort_heap(std::vector<T> &hT) {
    int size = hT.size();
    if (size > 0) {
        for (int i = size / 2 - 1; i >= 0; --i) {
            heapify(hT, i);
        }
    }
}

// public methods
template<typename T>
HeapDataStructure<T>::HeapDataStructure(): ptr(std::make_unique<std::vector<T>>()) {
}

template<typename T>
void HeapDataStructure<T>::insertNode(T val) {
    auto& hT = *ptr;
    hT.push_back(val);

    sort_heap(hT);
}

template<typename T>
bool HeapDataStructure<T>::deleteNode(T val) {
    auto& hT = *ptr;
    int size = hT.size();
    if (size == 0) {
        throw std::out_of_range("the heap is empty");
    }

    int m = -1;
    for (int i = 0; i < size; ++i)
    {
        if (val == hT[i]) {
            m = i;
            break;
        }
    }

    if (m >= 0) {
        swap(hT[m], hT[size - 1]);

        hT.pop_back();
        sort_heap(hT);

        return true;
    } else {
        return false;
    }
}

template<typename T>
T HeapDataStructure<T>::extract() {
    auto& hT = *ptr;
    int size = hT.size();
    if (size == 0) {
        throw std::out_of_range("the heap is empty");
    }

    std::swap(hT[0], hT[size - 1]);

    auto val = hT.back();

    hT.pop_back();
    sort_heap(hT);

    return val;
}

template<typename T>
T HeapDataStructure<T>::peek() {
    auto& hT = *ptr;
    int size = hT.size();
    if (size == 0) {
        throw std::out_of_range("the heap is empty");
    }
    return hT[0];
}

template<typename T>
const T& HeapDataStructure<T>::peek() const {
    auto& hT = *ptr;
    int size = hT.size();
    if (size == 0) {
        throw std::out_of_range("the heap is empty");
    }
    return hT[0];
}
}

template class ds::HeapDataStructure<int>;
