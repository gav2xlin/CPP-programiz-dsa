#ifndef HEAP_DATA_STRUCTURE_H
#define HEAP_DATA_STRUCTURE_H

#include "gtest/gtest.h"
#include <iostream>
#include <vector>
#include <memory>

namespace ds {
template<typename T>
class HeapDataStructure
{
private:
    FRIEND_TEST(HeapDataStructureTest, ExtractElement);
    FRIEND_TEST(HeapDataStructureTest, PeekElement);

    void swap(T& a, T& b);
    void heapify(std::vector<T> &hT, int i);
    void sort_heap(std::vector<T> &hT);

    std::unique_ptr<std::vector<T>> ptr;
public:
    HeapDataStructure();

    void insertNode(T val);
    bool deleteNode(T val);

    T peek();
    const T& peek() const;
    T extract();

    // https://en.cppreference.com/w/cpp/language/friend

    friend std::ostream& operator<<(std::ostream& os, const HeapDataStructure& hds) {
        auto& hT = *hds.ptr;
        for (int i = 0; i < hT.size(); ++i)
            os << hT[i] << " ";
        os << "\n";
        return os;
    }
};
}

#endif // HEAP_DATA_STRUCTURE_H
