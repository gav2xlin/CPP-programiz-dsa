cmake_minimum_required(VERSION 3.5)

project(heap_data_structure LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include(FetchContent)
FetchContent_Declare(
  googletest
  GIT_REPOSITORY https://github.com/google/googletest.git
  GIT_TAG        release-1.11.0
)
# For Windows: Prevent overriding the parent project's compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

set(HEAP_DATA_STRUCTURE_SOURCES heap_data_structure.cpp)
set(HEAP_DATA_STRUCTURE_HEADERS heap_data_structure.h)

add_executable(heap_data_structure_test ${HEAP_DATA_STRUCTURE_SOURCES} ${HEAP_DATA_STRUCTURE_HEADERS} test.cpp)
target_link_libraries(heap_data_structure_test gtest_main gmock)
add_test(NAME heap_data_structure_test COMMAND heap_data_structure_test)

add_executable(heap_data_structure ${HEAP_DATA_STRUCTURE_SOURCES} ${HEAP_DATA_STRUCTURE_HEADERS} main.cpp)
target_link_libraries(heap_data_structure gtest gmock)
