#include "heap_data_structure.h"
#include "gtest/gtest.h"
#include "gmock/gmock-matchers.h"
#include <vector>

namespace ds {
class HeapDataStructureTest : public testing::Test {
};

TEST_F(HeapDataStructureTest, ExtractFromEmpty) {
    HeapDataStructure<int> ht;

    EXPECT_THROW(ht.extract(), std::out_of_range);
}

TEST_F(HeapDataStructureTest, PeekFromEmpty) {
    HeapDataStructure<int> ht;

    EXPECT_THROW(ht.peek(), std::out_of_range);
}

TEST_F(HeapDataStructureTest, ExtractElement) {
    HeapDataStructure<int> ht;

    ht.insertNode(3);
    ht.insertNode(4);
    ht.insertNode(9);
    ht.insertNode(5);
    ht.insertNode(2);

    using ::testing::ElementsAreArray;

    EXPECT_EQ(9, ht.extract());
    EXPECT_THAT(*ht.ptr, ElementsAreArray({5, 3, 4, 2}));
}

TEST_F(HeapDataStructureTest, PeekElement) {
    HeapDataStructure<int> ht;

    ht.insertNode(3);
    ht.insertNode(4);
    ht.insertNode(9);
    ht.insertNode(5);
    ht.insertNode(2);
    using ::testing::ElementsAreArray;

    EXPECT_EQ(9, ht.peek());
    EXPECT_THAT(*ht.ptr, ElementsAreArray({9, 5, 4, 3, 2}));
}
}
