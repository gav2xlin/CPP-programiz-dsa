#include "heap_data_structure.h"
#include <iostream>

using namespace std;
using namespace ds;

int main (int argc, char *argv[]) {
    HeapDataStructure<int> heapTree;

    heapTree.insertNode(3);
    heapTree.insertNode(4);
    heapTree.insertNode(9);
    heapTree.insertNode(5);
    heapTree.insertNode(2);

    cout << "Max-Heap array: " << heapTree << endl;

    heapTree.deleteNode(4);

    cout << "After deleting an element: " << heapTree << endl;

    return 0;
}
