#include "doubly_linked_list.h"
#include <iostream>

using namespace std;
using namespace ds;

int main (int argc, char *argv[]) {
    DoublyLinkedList<int> list;

    DoublyLinkedList<int>::Node *n = list.insertFront(1);
    list.insertBack(3);

    cout << "all = " << list << endl;

    list.insertAfter(n, 2);

    n = list.search(2);

    cout << "all = " << list << endl;
    cout << "searched = " << n->data << endl;
    cout << "front = " << list.getFront()->data << endl;
    cout << "prev = " << list.getTail()->getPrev()->data << endl;
    cout << "tail = " << list.getTail()->data << endl;

    list.remove(n);

    cout << "all = " << list << endl;

    list.insertFront(0);

    n = list.search(1);

    cout << "all = " << list << endl;
    cout << "searched = " << n->data << endl;
    cout << "prev = " << n->getPrev()->data << endl;
    cout << "next = " << n->getNext()->data << endl;

    DoublyLinkedList<int> li{1, 2, 3};
    cout << "initializer list = " << li << endl;

    for (DoublyLinkedList<int>::Iterator it = li.begin(); it != li.end(); ++it)
    {
        cout << *it << endl;
    }

    for (auto v : li)
    {
        cout << v << endl;
    }

    return 0;
}
