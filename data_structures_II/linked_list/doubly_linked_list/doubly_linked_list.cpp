#include "doubly_linked_list.h"

namespace ds {
template <typename T>
DoublyLinkedList<T>::DoublyLinkedList(std::initializer_list<T> lst)
{
    for (auto &v : lst) {
        insertBack(v);
    }
}

template<typename T>
DoublyLinkedList<T>::~DoublyLinkedList()
{
    auto *c = head;
    while (c != NULL) {
        auto *t = c;
        c = c->next;
        delete t;
    }
}

template<typename T>
typename DoublyLinkedList<T>::Node* DoublyLinkedList<T>::getFront() noexcept
{
    return head;
}

template<typename T>
typename DoublyLinkedList<T>::Node* DoublyLinkedList<T>::getTail() noexcept
{
    return tail;
}

template<typename T>
typename DoublyLinkedList<T>::Node* DoublyLinkedList<T>::insertFront(T v)
{
    auto *c = new typename DoublyLinkedList<T>::Node;

    c->data = v;
    c->next = head;
    if (head) {
        head->prev = c;
    }
    head = c;

    if (tail == NULL) {
        tail = c;
    }

    return c;
}

template<typename T>
typename DoublyLinkedList<T>::Node* DoublyLinkedList<T>::insertBack(T v)
{
    auto *c = new typename DoublyLinkedList<T>::Node;

    c->data = v;
    if (tail) {
        tail->next = c;
    }
    c->prev = tail;
    tail = c;

    if (head == NULL) {
        head = c;
    }

    return c;
}

template<typename T>
typename DoublyLinkedList<T>::Node* DoublyLinkedList<T>::insertAfter(DoublyLinkedList<T>::Node* p, T v)
{
    auto *c = new typename DoublyLinkedList<T>::Node;

    c->data = v;

    if (p != NULL) {
        auto *n = p->next;
        p->next = c;
        c->next = n;
        c->prev = p;
        n->prev = c;

        if (tail == NULL) {
            tail = c;
        }
    } else {
        head = tail = c;
    }

    return c;
}

template<typename T>
bool DoublyLinkedList<T>::remove(typename DoublyLinkedList<T>::Node* s)
{
    if (s == NULL) {
        return false;
    }

    if (s == head) {
        head = s->next;

        if (head == NULL) {
            tail = NULL;
        }

        delete s;

        return true;
    }

    auto *c = head;
    while (c != NULL) {
        auto *p = c;
        c = c->next;

        if (c == s) {
            auto *n = c->next;
            p->next = n;
            n->prev = p;

            if (tail == c) {
                tail = p;
            }

            delete c;

            return true;
        }
    }

    return false;
}

template<typename T>
typename DoublyLinkedList<T>::Node* DoublyLinkedList<T>::search(T v) noexcept
{
    auto *c = head;
    while (c != NULL) {
        if (c->data == v) return c;
        c = c->next;
    }

    return NULL;
}
}

template class ds::DoublyLinkedList<int>;
