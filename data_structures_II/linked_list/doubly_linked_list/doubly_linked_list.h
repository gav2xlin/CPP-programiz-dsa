#ifndef DOUBLY_LINKED_LIST_H
#define DOUBLY_LINKED_LIST_H

#include <cstddef>
#include <iostream>
#include <initializer_list>

namespace ds {
template<typename T>
class DoublyLinkedList
{
public:
    DoublyLinkedList() = default;
    explicit DoublyLinkedList(std::initializer_list<T> lst);
    ~DoublyLinkedList();

    class Node
    {
    private:
        Node *next{0}, *prev{0};
    public:
        Node* getPrev() {
            return prev;
        }

        Node* getNext() {
            return next;
        }

        T data;

        friend class DoublyLinkedList;
    };

    Node* getFront() noexcept;
    Node* getTail() noexcept;

    Node* insertFront(T v);
    Node* insertBack(T v);
    Node* insertAfter(Node* p, T v);

    bool remove(Node* n);
    Node* search(T v) noexcept;

    class Iterator
    {
    public:
        Iterator(const Node* node) noexcept : mPtr (node) { }

        Iterator& operator=(Node* pNode)
        {
            this->mPtr = pNode;
            return *this;
        }

        Iterator& operator++()
        {
            if (mPtr) mPtr = mPtr->next;
            return *this;
        }

        Iterator& operator--()
        {
            if (mPtr) mPtr = mPtr->prev;
            return *this;
        }

        Iterator operator++(int)
        {
            Iterator iterator = *this;
            ++*this;
            return iterator;
        }

        Iterator operator--(int)
        {
            Iterator iterator = *this;
            ++*this;
            return iterator;
        }

        bool operator==(const Iterator& it)
        {
            return mPtr == it.mPtr;
        }

        bool operator!=(const Iterator& it)
        {
            return mPtr != it.mPtr;
        }

        int operator*()
        {
            return mPtr->data;
        }

    private:
        const Node* mPtr;
    };

    Iterator begin()
    {
        return Iterator(head);
    }

    Iterator end()
    {
        return Iterator(nullptr);
    }

    // https://en.cppreference.com/w/cpp/language/friend

    friend std::ostream& operator<<(std::ostream& os, const DoublyLinkedList& list) {
        auto *c = list.head;
        while (c != NULL) {
            os << c->data << " ";
            c = c->next;
        }
        return os;
    }
protected:
    Node *head{0}, *tail{0};
};
}

#endif // DOUBLY_LINKED_LIST_H
