set(DOUBLY_LIST_SOURCES doubly_linked_list.cpp)
set(DOUBLY_LIST_HEADERS doubly_linked_list.h)

add_executable(doubly_linked_list ${DOUBLY_LIST_SOURCES} ${DOUBLY_LIST_HEADERS} doubly_linked_list_main.cpp)
target_link_libraries(doubly_linked_list gtest gmock)

add_executable(doubly_linked_list_test ${DOUBLY_LIST_SOURCES} ${DOUBLY_LIST_HEADERS} doubly_linked_list_test.cpp)
target_link_libraries(doubly_linked_list_test gtest_main gmock)
add_test(NAME doubly_linked_list COMMAND doubly_linked_list)
