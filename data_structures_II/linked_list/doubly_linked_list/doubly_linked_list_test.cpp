#include "doubly_linked_list.h"
#include "doubly_linked_list.cpp"
#include "gtest/gtest.h"
#include "gmock/gmock-matchers.h"
#include <vector>

namespace ds {
class DoublyLinkedListTest : public testing::Test {
protected:
    DoublyLinkedList<int> li;
};

TEST_F(DoublyLinkedListTest, EmptyList) {
    EXPECT_EQ(NULL, li.getFront());
    EXPECT_EQ(NULL, li.getTail());
}

TEST_F(DoublyLinkedListTest, InsertElements) {
    auto *head = li.insertFront(1);
    auto *tail = li.insertBack(3);
    auto *middle = li.insertAfter(head, 2);

    EXPECT_EQ(head, li.getFront());
    EXPECT_EQ(middle, li.getFront()->getNext());
    EXPECT_EQ(NULL, li.getFront()->getPrev());
    EXPECT_EQ(tail, li.getTail());
    EXPECT_EQ(middle, li.getTail()->getPrev());
    EXPECT_EQ(NULL, li.getTail()->getNext());

    EXPECT_EQ(1, head->data);
    EXPECT_EQ(2, middle->data);
    EXPECT_EQ(3, tail->data);
}

TEST_F(DoublyLinkedListTest, SearchElement) {
    li.insertBack(1);
    auto *node = li.insertBack(2);
    li.insertBack(3);

    auto *found = li.search(2);

    EXPECT_EQ(found, node);
}

TEST_F(DoublyLinkedListTest, RemoveElement) {
    li.insertBack(1);
    auto *node = li.insertBack(2);
    li.insertBack(3);

    li.remove(node);

    EXPECT_EQ(li.getTail(), li.getFront()->getNext());
    EXPECT_EQ(li.getFront(), li.getTail()->getPrev());
    EXPECT_EQ(NULL, li.getFront()->getPrev());
    EXPECT_EQ(NULL, li.getTail()->getNext());
}

TEST_F(DoublyLinkedListTest, InitializerList) {
    DoublyLinkedList<int> li{1, 2, 3, 4, 5};

    using ::testing::ElementsAreArray;

    std::vector<int> vec;
    for (auto v : li) {
        vec.push_back(v);
    }

    EXPECT_THAT(vec, ElementsAreArray({1, 2, 3, 4, 5}));
}
}
