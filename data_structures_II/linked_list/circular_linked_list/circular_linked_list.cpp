#include "circular_linked_list.h"

namespace ds {
template <typename T>
CircularLinkedList<T>::CircularLinkedList(std::initializer_list<T> lst)
{
    for (auto &v : lst) {
        insertBack(v);
    }
}

template<typename T>
CircularLinkedList<T>::~CircularLinkedList()
{
    auto *c = head;
    while (c != tail) {
        auto *t = c;
        c = c->next;
        delete t;
    }
    if (tail) {
        delete tail;
    }
}

template<typename T>
typename CircularLinkedList<T>::Node* CircularLinkedList<T>::getFront() noexcept
{
    return head;
}

template<typename T>
typename CircularLinkedList<T>::Node* CircularLinkedList<T>::getTail() noexcept
{
    return tail;
}

template<typename T>
typename CircularLinkedList<T>::Node* CircularLinkedList<T>::insertFront(T v)
{
    auto *c = new typename CircularLinkedList<T>::Node;

    c->data = v;
    c->next = head;
    head = c;

    if (tail == NULL) {
        tail = c;
    }

    tail->next = head;

    return c;
}

template<typename T>
typename CircularLinkedList<T>::Node* CircularLinkedList<T>::insertBack(T v)
{
    auto *c = new typename CircularLinkedList<T>::Node;

    c->data = v;
    if (tail) {
        tail->next = c;
    }
    tail = c;

    if (head == NULL) {
        head = c;
    }

    c->next = head;

    return c;
}

template<typename T>
typename CircularLinkedList<T>::Node* CircularLinkedList<T>::insertAfter(typename CircularLinkedList<T>::Node* p, T v)
{
    auto *c = new typename CircularLinkedList<T>::Node;

    c->data = v;

    if (p != NULL) {
        auto *n = p->next;
        p->next = c;
        c->next = n;

        if (tail == NULL) {
            tail = c;
        }
    } else {
        head = tail = c;
        c->next = c;
    }

    return c;
}


template<typename T>
bool CircularLinkedList<T>::remove(typename CircularLinkedList<T>::Node* s)
{
    if (s == NULL) {
        return false;
    }

    if (s == head) {
        head = s->next;
        tail->next = head;

        if (head == tail) {
            head = tail = NULL;
        }

        delete s;

        return true;
    }

    auto *c = head;
    while (c != NULL) {
        auto *p = c;
        c = c->next;

        if (c == s) {
            auto *n = c->next;
            p->next = n;

            if (tail == c) {
                tail = p;
                tail->next = head;
            }

            delete c;

            return true;
        }
    }

    return false;
}

template<typename T>
typename CircularLinkedList<T>::Node* CircularLinkedList<T>::search(T v) noexcept
{
    auto *c = head;
    while (c != NULL) {
        if (c->data == v) return c;
        c = c->next;
    }

    return NULL;
}
}

template class ds::CircularLinkedList<int>;
