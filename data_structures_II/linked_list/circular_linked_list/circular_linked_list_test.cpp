#include "circular_linked_list.h"
#include "circular_linked_list.cpp"
#include "gtest/gtest.h"
#include "gmock/gmock-matchers.h"

namespace ds {
class CircularLinkedListTest : public testing::Test {
protected:
    CircularLinkedList<int> li;
};

TEST_F(CircularLinkedListTest, EmptyList) {
    EXPECT_EQ(NULL, li.getFront());
    EXPECT_EQ(NULL, li.getTail());
}

TEST_F(CircularLinkedListTest, InsertElements) {
    auto *head = li.insertFront(1);
    auto *tail = li.insertBack(3);
    auto *middle = li.insertAfter(head, 2);

    EXPECT_EQ(head, li.getFront());
    EXPECT_EQ(middle, li.getFront()->getNext());
    EXPECT_EQ(tail, li.getTail());
    EXPECT_EQ(head, li.getTail()->getNext());

    EXPECT_EQ(1, head->data);
    EXPECT_EQ(2, middle->data);
    EXPECT_EQ(3, tail->data);
}

TEST_F(CircularLinkedListTest, SearchElement) {
    li.insertBack(1);
    auto *node = li.insertBack(2);
    li.insertBack(3);

    auto *found = li.search(2);

    EXPECT_EQ(found, node);
}

TEST_F(CircularLinkedListTest, RemoveElement) {
    li.insertBack(1);
    auto *node = li.insertBack(2);
    li.insertBack(3);

    li.remove(node);

    EXPECT_EQ(li.getTail(), li.getFront()->getNext());
    EXPECT_EQ(li.getFront(), li.getTail()->getNext());
}

TEST_F(CircularLinkedListTest, InitializerList) {
    CircularLinkedList<int> li{1, 2, 3, 4, 5};

    using ::testing::ElementsAreArray;

    std::vector<int> vec;
    for (CircularLinkedList<int>::Iterator it = li.begin(); it != li.getTail(); ++it)
    {
        vec.push_back(*it);
    }
    vec.push_back(li.getTail()->data);

    EXPECT_THAT(vec, ElementsAreArray({1, 2, 3, 4, 5}));
}
}
