set(CIRCULAR_LIST_SOURCES circular_linked_list.cpp)
set(CIRCULAR_LIST_HEADERS circular_linked_list.h)

add_executable(circular_linked_list ${CIRCULAR_LIST_SOURCES} ${CIRCULAR_LIST_HEADERS} circular_linked_list_main.cpp)
target_link_libraries(circular_linked_list gtest gmock)

add_executable(circular_linked_list_test ${CIRCULAR_LIST_SOURCES} ${CIRCULAR_LIST_HEADERS} circular_linked_list_test.cpp)
target_link_libraries(circular_linked_list_test gtest_main gmock)
add_test(NAME circular_linked_list COMMAND circular_linked_list)
