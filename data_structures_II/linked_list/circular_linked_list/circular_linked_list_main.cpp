#include "circular_linked_list.h"
#include <iostream>

using namespace std;
using namespace ds;

int main (int argc, char *argv[]) {
    CircularLinkedList<int> list;

    CircularLinkedList<int>::Node *n = list.insertFront(1);
    list.insertBack(3);
    list.insertAfter(n, 2);

    n = list.search(2);

    std::cout << "all = " << list << std::endl;
    std::cout << "searched = " << n->data << std::endl;
    std::cout << "front = " << list.getFront()->data << std::endl;
    std::cout << "tail = " << list.getTail()->data << std::endl;

    list.remove(n);

    std::cout << "all = " << list << std::endl;

    list.insertFront(0);

    std::cout << "all = " << list << std::endl;

    CircularLinkedList<int> li{1, 2, 3};
    std::cout << "initializer list = " << li << std::endl;

    for (CircularLinkedList<int>::Iterator it = li.begin(); it != li.getTail(); ++it)
    {
        cout << *it << endl;
    }
    cout << *CircularLinkedList<int>::Iterator(li.getTail()) << endl;

    // an infinite loop
    /*for (auto v : li)
    {
        cout << v << endl;
    }*/

    return 0;
}
