#include "singly_linked_list.h"

namespace ds {
template<typename T>
SinglyLinkedList<T>::SinglyLinkedList(std::initializer_list<T> lst)
{
    for (auto &v : lst) {
        insertBack(v);
    }
}

template<typename T>
SinglyLinkedList<T>::~SinglyLinkedList()
{
    auto *c = head;
    while (c != NULL) {
        auto *t = c;
        c = c->next;
        delete t;
    }
}

template<typename T>
typename SinglyLinkedList<T>::Node* SinglyLinkedList<T>::getFront() noexcept
{
    return head;
}

template<typename T>
typename SinglyLinkedList<T>::Node* SinglyLinkedList<T>::getTail() noexcept
{
    return tail;
}

template<typename T>
typename SinglyLinkedList<T>::Node* SinglyLinkedList<T>::insertFront(T v)
{
    auto *c = new typename SinglyLinkedList<T>::Node;

    c->data = v;
    c->next = head;
    head = c;

    if (tail == NULL) {
        tail = c;
    }

    return c;
}

template<typename T>
typename SinglyLinkedList<T>::Node* SinglyLinkedList<T>::insertBack(T v)
{
    auto *c = new typename SinglyLinkedList<T>::Node;

    c->data = v;
    if (tail) {
        tail->next = c;
    }
    tail = c;

    if (head == NULL) {
        head = c;
    }

    return c;
}

template<typename T>
typename SinglyLinkedList<T>::Node* SinglyLinkedList<T>::insertAfter(typename SinglyLinkedList<T>::Node* p, T v)
{
    auto *c = new typename SinglyLinkedList<T>::Node;

    c->data = v;

    if (p != NULL) {
        auto *n = p->next;
        p->next = c;
        c->next = n;

        if (tail == NULL) {
            tail = c;
        }
    } else {
       head = tail = c;
    }

    return c;
}

template<typename T>
bool SinglyLinkedList<T>::remove(typename SinglyLinkedList<T>::Node* s)
{
    if (s == NULL) {
        return false;
    }

    if (s == head) {
        head = s->next;

        if (head == NULL) {
            tail = NULL;
        }

        delete s;

        return true;
    }

    auto *c = head;
    while (c != NULL) {
        auto *p = c;
        c = c->next;

        if (c == s) {
            auto *n = c->next;
            p->next = n;

            if (tail == c) {
                tail = p;
            }

            delete c;

            return true;
        }
    }

    return false;
}

template<typename T>
typename SinglyLinkedList<T>::Node* SinglyLinkedList<T>::search(T v) noexcept
{
    auto *c = head;
    while (c != NULL) {
        if (c->data == v) return c;
        c = c->next;
    }

    return NULL;
}
}

template class ds::SinglyLinkedList<int>;
