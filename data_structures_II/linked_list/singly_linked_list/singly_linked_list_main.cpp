#include "singly_linked_list.h"
#include <iostream>

using namespace std;
using namespace ds;

int main (int argc, char *argv[]) {
    SinglyLinkedList<int> list;

    SinglyLinkedList<int>::Node *n = list.insertFront(1);
    list.insertBack(3);
    list.insertAfter(n, 2);

    n = list.search(2);

    cout << "all = " << list << endl;
    cout << "searched = " << n->data << endl;
    cout << "front = " << list.getFront()->data << endl;
    cout << "tail = " << list.getTail()->data << endl;

    list.remove(n);
    list.insertFront(0);

    cout << "all = " << list << endl;

    SinglyLinkedList<int> li{1, 2, 3};
    cout << "initializer list = " << li << endl;

    for (SinglyLinkedList<int>::Iterator it = li.begin(); it != li.end(); ++it)
    {
        cout << *it << endl;
    }

    for (auto v : li)
    {
        cout << v << endl;
    }

    return 0;
}
