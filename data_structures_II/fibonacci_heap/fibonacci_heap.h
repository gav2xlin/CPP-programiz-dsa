#ifndef FIBONACCIHEAP_H
#define FIBONACCIHEAP_H

//#include "gtest/gtest.h"
#include <iostream>
#include <cstddef>

namespace ds {
template<typename T>
class FibonacciHeap {
public:
    class Node {
    private:
        T value;
        Node* prev{nullptr};
        Node* next{nullptr};
        Node* parent{nullptr};
        Node* child{nullptr};
        int degree{0};
        bool marked{false};
    public:
        friend class FibonacciHeap;

        T getValue();
        const Node* getPrev();
        const Node* getNext();
        const Node* getParent();
        const Node* getChild();
        bool isMarked();
        bool hasParent();
        bool hasChildren();
    };

    FibonacciHeap() = default;
    ~FibonacciHeap();

    bool isEmpty();
    T getMinimum();
    Node* insertNode(T value);
    void unionHeap(FibonacciHeap& other);
    Node* const findNode(T value) const;
    T extractMinimum();
    void decreaseKey(Node* node, T value);

    // https://en.cppreference.com/w/cpp/language/friend

    friend std::ostream& operator<<(std::ostream& os, const FibonacciHeap& fh) {
        Node *p = fh.heap;
        if (p == nullptr) {
            os << "The heap is empty" << std::endl;
            return os;
        }

        fh.printAll(os, fh.heap);
        os << std::endl;

        return os;
    }
private:
    Node* heap{nullptr};
    int nH{0};

    void deleteAll(Node* node);
    Node* createNode(T value);
    void swapNodesIf(Node* &a, Node* &b);
    Node* unionNodes(Node* a, Node* b);
    Node* _findNode(Node* node, T value) const;
    void unMarkAndUnParentAll(Node* node);
    void addChild(Node* parent, Node* child);
    Node* _extractMinimum(Node* node);
    Node* _decreaseKey(Node* heap, Node* node, T value);
    Node* cut(Node* heap, Node* node);
    void printAll(std::ostream& os, Node* node) const;
};
}

#endif // FIBONACCIHEAP_H
