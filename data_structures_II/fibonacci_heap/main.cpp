#include "fibonacci_heap.h"
#include <iostream>

using namespace std;
using namespace ds;

int main() {
    FibonacciHeap<int> fh;

    fh.insertNode(7);
    fh.insertNode(17);
    fh.insertNode(26);
    fh.insertNode(5);
    fh.insertNode(1);

    cout << fh << endl;
    cout << "minimum: " << fh.getMinimum() << endl;

    cout << "extracted: " << fh.extractMinimum() << endl;
    cout << "minimum: " << fh.getMinimum() << endl;

    cout << fh << endl;

    cout << "find node: ";
    auto* n = fh.findNode(26);
    cout << n->getValue() << endl;

    cout << "decrease key" << endl;
    fh.decreaseKey(n, 16);

    cout << fh << endl;

    for (int i = 0; i < 3; ++i) {
        cout << "extracted: " << fh.extractMinimum() << endl;
        cout << "minimum: " << fh.getMinimum() << endl;
        cout << fh << endl;
    }

    return 0;
}
