#include "fibonacci_heap.h"
#include <stdexcept>

namespace ds {
// private methods

template<typename T>
void FibonacciHeap<T>::deleteAll(Node* node) {
    if(node != nullptr) {
        Node* cur = node;
        do {
            Node* p = cur;
            deleteAll(p->child);
            cur = cur->next;
            delete p;
        } while(cur != node);
    }
}

template<typename T>
void FibonacciHeap<T>::printAll(std::ostream& os, Node* node) const {
    if(node != nullptr) {
        Node* cur = node;
        do {
            os << cur->value;
            cur = cur->next;

            if (cur->parent != nullptr) {
                os << '(' << cur->parent->value << ')';
            }

            if (cur != node) {
                os << "-->";
            }
        } while(cur != node);
        os << std::endl;

        cur = node;
        do {
            printAll(os, cur->child);

            cur = cur->next;
        } while(cur != node);
    }
}

template<typename T>
typename FibonacciHeap<T>::Node* FibonacciHeap<T>::createNode(T value) {
    auto* node = new Node;
    node->value = value;
    node->prev = node->next = node;
    ++nH;
    return node;
}

template<typename T>
void FibonacciHeap<T>::swapNodesIf(Node* &a, Node* &b) {
    if(a->value > b->value) {
        auto* temp = a;
        a = b;
        b = temp;
    }
}

template<typename T>
typename FibonacciHeap<T>::Node* FibonacciHeap<T>::unionNodes(Node* a, Node* b) {
    if (a == nullptr) return b;
    if (b == nullptr) return a;

    swapNodesIf(a, b);

    auto* an = a->next;
    auto* bp = b->prev;

    a->next = b; // a->b
    bp->next = an; // bp->an

    b->prev = a; // a<-b
    an->prev = bp; // bp<-an

    return a;
}

template<typename T>
typename FibonacciHeap<T>::Node* FibonacciHeap<T>::_findNode(Node* node, T value) const {
    if (node == nullptr) return NULL;

    Node* cur = node;
    do {
        if (cur->value == value) return cur;
        Node* res = _findNode(cur->child, value);
        if (res) return res;
        cur = cur -> next;
    } while (cur != node);

    return nullptr;
}

template<typename T>
void FibonacciHeap<T>::unMarkAndUnParentAll(Node* node) {
    if (node == nullptr) return;

    Node* cur = node;
    do {
        cur->marked = false;
        cur->parent = nullptr;

        cur = cur->next;
    } while (cur != node);
}

template<typename T>
void FibonacciHeap<T>::addChild(Node* parent, Node* child) {
    child->prev = child->next = child; // reset links
    child->parent = parent;
    parent->child = unionNodes(parent->child, child); // parent->child <- child or child <- parent->child
    ++parent->degree;
}

template<typename T>
typename FibonacciHeap<T>::Node* FibonacciHeap<T>::_extractMinimum(Node* node) {
    unMarkAndUnParentAll(node->child);

    // the node is removed in extractMinimum
    if(node->next == node) {
        node = node->child; // p -> c (remove the last top node)
    } else {
        // ap - a - n - b - bn -> // ap - a - b - bn
        node->next->prev = node->prev;
        node->prev->next = node->next;
        node = unionNodes(node->next, node->child);
    }
    if (node == nullptr) return node;

    //int size = static_cast<int>(log(nH) / log(2));
    //Node** trees = new Node*[size]{nullptr};
    Node** trees = new Node*[64]{nullptr};

    while (true) {
        if (trees[node->degree] != nullptr) { // the same degree, but previous and current nodes
            Node* t = trees[node->degree]; // get a previous node
            if (t == node) break; // break an infinite lopp

            trees[node->degree] = nullptr; // reset

            // ab - a - t - b - bn -> ab - a - b - bn
            t->prev->next = t->next;
            t->next->prev = t->prev;

            if(node->value < t->value) {
                addChild(node, t); // node.child = c1 -> c2 ... -> t
            } else {
                if (node->next == node) {
                    t->next = t->prev = t; // single and circular
                } else {
                    // np - n - nn -> np - t - nn
                    node->prev->next = t;
                    node->next->prev = t;
                    t->next = node->next;
                    t->prev = node->prev;
                }

                addChild(t, node); // t.child = c1 -> c2 ... -> node
                node = t; // node > t
            }

            continue;
        } else {
            trees[node->degree] = node; // degree -> node
        }

        node = node->next;
    }

    // find a minimum node
    Node* min = node;
    Node* start = node;
    do {
        if(node->value < min->value) min = node;
        node = node->next;
    } while(node != start);

    delete[] trees;
    --nH;

    return min;
}

template<typename T>
typename FibonacciHeap<T>::Node* FibonacciHeap<T>::cut(Node* heap, Node* node) {
    if(node->next == node) {
        node->parent->child = nullptr; // the last node
    } else {
        // exclude the node: a <- n <- b
        node->next->prev = node->prev;
        node->prev->next = node->next;
        // the list is circlular
        node->parent->child = node->next;
    }

    // reset links
    node->next = node->prev = node;
    node->marked = false;

    return unionNodes(heap, node); // the root list: heap <- node or node <- heap
}

template<typename T>
typename FibonacciHeap<T>::Node* FibonacciHeap<T>::_decreaseKey(Node* heap, Node* node, T value) {
    if (node->value < value) return heap;
    node->value=value;

    if (node->parent) {
        if (node->value < node->parent->value) {
            heap = cut(heap, node);

            Node* parent = node->parent;
            node->parent = nullptr;

            // lift marked parents to the root list
            while (parent != nullptr && parent->marked) {
                heap = cut(heap, parent);
                node = parent;
                parent = node->parent;
                node->parent = nullptr;
            }

            if (parent != nullptr && parent->parent != nullptr) parent->marked = true;
        }
    } else if (node->value < heap->value) {
        heap = node;
    }

    return heap;
}

// public node methods

template<typename T>
T FibonacciHeap<T>::Node::getValue() {
    return value;
}

template<typename T>
const typename FibonacciHeap<T>::Node* FibonacciHeap<T>::Node::getPrev() {
    return prev;
}

template<typename T>
const typename FibonacciHeap<T>::Node* FibonacciHeap<T>::Node::getNext() {
    return next;
}

template<typename T>
const typename FibonacciHeap<T>::Node* FibonacciHeap<T>::Node:: getParent() {
    return parent;
}

template<typename T>
const typename FibonacciHeap<T>::Node* FibonacciHeap<T>::Node::getChild() {
    return child;
}

template<typename T>
bool FibonacciHeap<T>::Node::isMarked() {
    return marked;
}

template<typename T>
bool FibonacciHeap<T>::Node::hasParent() {
    return parent;
}

template<typename T>
bool FibonacciHeap<T>::Node::hasChildren() {
    return child;
}

// public methods

template<typename T>
FibonacciHeap<T>::~FibonacciHeap() {
    deleteAll(heap);
}

template<typename T>
bool FibonacciHeap<T>::isEmpty() {
    return heap == nullptr;
}

template<typename T>
T FibonacciHeap<T>::getMinimum() {
    if (heap == nullptr) {
        throw std::out_of_range("The heap is empty");
    }

    return heap->value;
}

template<typename T>
typename FibonacciHeap<T>::Node* FibonacciHeap<T>::insertNode(T value) {
    auto* node = createNode(value);
    heap = unionNodes(heap, node);
    return node;
}

template<typename T>
void FibonacciHeap<T>::unionHeap(FibonacciHeap& other) {
    heap = unionNodes(heap, other.heap);
    other.heap = nullptr;
}

template<typename T>
typename FibonacciHeap<T>::Node* const FibonacciHeap<T>::findNode(T value) const {
  return _findNode(heap, value);
}

template<typename T>
T FibonacciHeap<T>::extractMinimum() {
    if (heap == nullptr) {
        throw std::out_of_range("The heap is empty");
    }

    Node* prev = heap;
    heap = _extractMinimum(heap);
    T res = prev->value;
    delete prev;

    return res;
}

template<typename T>
void FibonacciHeap<T>::decreaseKey(Node* node, T value) {
    if (heap == nullptr) {
        throw std::out_of_range("The heap is empty");
    }

    heap = _decreaseKey(heap, node, value);
}
}

template class ds::FibonacciHeap<int>;
