#ifndef DEQUEUE_H
#define DEQUEUE_H

#include "gtest/gtest.h"
#include <iostream>

namespace ds {
template<int sz, typename T>
class Dequeue;

template<int sz, typename T>
std::ostream& operator<<(std::ostream&, const Dequeue<sz, T>&);

template<int sz, typename T>
class Dequeue {
private:
    FRIEND_TEST(DequeueTest, InsertFrontToFull);
    FRIEND_TEST(DequeueTest, InsertRearToFull);
    FRIEND_TEST(DequeueTest, IsQueueFull);
    FRIEND_TEST(DequeueTest, ClearElements);
    FRIEND_TEST(DequeueTest, PeekElements);
    FRIEND_TEST(DequeueTest, InsertFrontElements);
    FRIEND_TEST(DequeueTest, InsertRearElements);
    FRIEND_TEST(DequeueTest, DeleteFrontElements);
    FRIEND_TEST(DequeueTest, DeleteRearElements);

    T items[sz];
    int front{-1}, rear{-1};
public:
    void insertFront(T v);
    void insertRear(T v);
    void deleteFront();
    void deleteRear();
    bool isEmpty() const;
    bool isFull() const;
    T& peekFront();
    const T& peekFront() const;
    T& peekRear();
    const T& peekRear() const;
    void clear();
    int cap() const;
    int len() const;

    // https://en.cppreference.com/w/cpp/language/friend Template friend operators

    // refers to a full specialization for this particular T
    // note: this relies on template argument deduction in declarations
    // can also specify the template argument with operator<< <T>"

    friend std::ostream& operator<< <>(std::ostream& os, const Dequeue &);
};
}

#endif // DEQUEUE_H
