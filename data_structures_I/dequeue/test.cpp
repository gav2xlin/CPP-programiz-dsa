#include "dequeue.h"
#include "dequeue.cpp"
#include "gtest/gtest.h"
#include "gmock/gmock-matchers.h"
#include <vector>

namespace ds {
class DequeueTest : public testing::Test {
};

TEST_F(DequeueTest, EmptyQueue) {
    Dequeue<10, int> queue;

    EXPECT_EQ(0, queue.len());
    EXPECT_EQ(10, queue.cap());
    EXPECT_TRUE(queue.isEmpty());
    EXPECT_FALSE(queue.isFull());
}

TEST_F(DequeueTest, InsertFrontToFull) {
    Dequeue<10, int> queue;

    queue.front = 0;
    queue.rear = 9;

    EXPECT_THROW(queue.insertFront(10), std::out_of_range);
}

TEST_F(DequeueTest, InsertRearToFull) {
    Dequeue<10, int> queue;

    queue.front = 0;
    queue.rear = 9;

    EXPECT_THROW(queue.insertRear(10), std::out_of_range);
}

TEST_F(DequeueTest, IsQueueFull) {
    Dequeue<10, int> queue;

    queue.front = 5;
    queue.rear = 4;

    EXPECT_TRUE(queue.isFull());
}

TEST_F(DequeueTest, ClearElements) {
    Dequeue<10, int> queue;

    int v[]{1, 2, 3, 4, 5};
    std::copy(std::begin(v), std::end(v), std::begin(queue.items));
    queue.front = 0;
    queue.rear = 4;

    queue.clear();

    EXPECT_EQ(0, queue.len());
    EXPECT_EQ(10, queue.cap());
    EXPECT_TRUE(queue.isEmpty());
    EXPECT_FALSE(queue.isFull());
}

TEST_F(DequeueTest, DeleteFrontFromEmpty) {
    Dequeue<10, int> queue;

    EXPECT_THROW(queue.deleteFront(), std::out_of_range);
}

TEST_F(DequeueTest, DeleteRearFromEmpty) {
    Dequeue<10, int> queue;

    EXPECT_THROW(queue.deleteRear(), std::out_of_range);
}

TEST_F(DequeueTest, PeekElements) {
    Dequeue<10, int> queue;

    int v[]{1, 2, 3, 4, 5};
    std::copy(std::begin(v), std::end(v), std::begin(queue.items));
    queue.front = 0;
    queue.rear = 4;

    queue.deleteFront();
    queue.deleteRear();

    EXPECT_EQ(2, queue.peekFront());
    EXPECT_EQ(4, queue.peekRear());
}

TEST_F(DequeueTest, InsertFrontElements) {
    Dequeue<10, int> queue;

    int v[]{2, 3, 4, 5, 6, 7, 8, 9};
    std::copy(std::begin(v), std::end(v), std::begin(queue.items) + 1);
    queue.front = 1;
    queue.rear = 8;

    queue.insertFront(1);
    queue.insertFront(10);

    using ::testing::ElementsAreArray;

    EXPECT_EQ(10, queue.len());
    EXPECT_EQ(10, queue.cap());
    EXPECT_FALSE(queue.isEmpty());
    EXPECT_TRUE(queue.isFull());
    EXPECT_THAT(std::vector(std::begin(queue.items), std::end(queue.items)), ElementsAreArray({1, 2, 3, 4, 5, 6, 7, 8, 9, 10}));
}

TEST_F(DequeueTest, InsertRearElements) {
    Dequeue<10, int> queue;

    int v[]{2, 3, 4, 5, 6, 7, 8, 9};
    std::copy(std::begin(v), std::end(v), std::begin(queue.items) + 1);
    queue.front = 1;
    queue.rear = 8;

    queue.insertRear(10);
    queue.insertRear(1);

    using ::testing::ElementsAreArray;

    EXPECT_EQ(10, queue.len());
    EXPECT_EQ(10, queue.cap());
    EXPECT_FALSE(queue.isEmpty());
    EXPECT_TRUE(queue.isFull());
    EXPECT_THAT(std::vector(std::begin(queue.items), std::end(queue.items)), ElementsAreArray({1, 2, 3, 4, 5, 6, 7, 8, 9, 10}));
}

TEST_F(DequeueTest, DeleteFrontElements) {
    Dequeue<10, int> queue;

    int v[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::copy(std::begin(v), std::end(v), std::begin(queue.items));
    queue.front = 8;
    queue.rear = 7;

    for (int i = 0; i < 4; ++i) {
        queue.deleteFront();
    }

    using ::testing::ElementsAreArray;

    EXPECT_EQ(6, queue.len());
    EXPECT_EQ(10, queue.cap());
    EXPECT_FALSE(queue.isEmpty());
    EXPECT_FALSE(queue.isFull());
    EXPECT_THAT(std::vector(std::begin(queue.items) + queue.front, std::begin(queue.items) + queue.rear + 1), ElementsAreArray({3, 4, 5, 6, 7, 8}));
}

TEST_F(DequeueTest, DeleteRearElements) {
    Dequeue<10, int> queue;

    int v[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::copy(std::begin(v), std::end(v), std::begin(queue.items));
    queue.front = 2;
    queue.rear = 1;

    for (int i = 0; i < 4; ++i) {
        queue.deleteRear();
    }

    using ::testing::ElementsAreArray;

    EXPECT_EQ(6, queue.len());
    EXPECT_EQ(10, queue.cap());
    EXPECT_FALSE(queue.isEmpty());
    EXPECT_FALSE(queue.isFull());
    EXPECT_THAT(std::vector(std::begin(queue.items) + queue.front, std::begin(queue.items) + queue.rear + 1), ElementsAreArray({3, 4, 5, 6, 7, 8}));
}
}
