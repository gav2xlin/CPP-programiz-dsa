#include "dequeue.h"
#include <stdexcept>

namespace ds {
template<int sz, typename T>
void Dequeue<sz, T>::insertFront(T v) {
    if (isFull()) {
        throw std::out_of_range("the queue is full");
    }
    if (front < 0) {
        front = rear = 0;
    } else {
        front = front > 0 ? front - 1 : sz - 1;
    }
    items[front] = v;
}

template<int sz, typename T>
void Dequeue<sz, T>::insertRear(T v) {
    if (isFull()) {
        throw std::out_of_range("the queue is full");
    }
    if (front < 0) {
        front = rear = 0;
    } else {
        rear = (rear + 1) % sz;
    }
    items[rear] = v;
}

template<int sz, typename T>
void Dequeue<sz, T>::deleteFront() {
    if (isEmpty()) {
        throw std::out_of_range("the queue is empty");
    }
    if (front == rear) {
        front = rear = -1;
    } else {
        front = (front + 1) % sz;
    }
}

template<int sz, typename T>
void Dequeue<sz, T>::deleteRear() {
    if (isEmpty()) {
        throw std::out_of_range("the queue is empty");
    }
    if (front == rear) {
        front = rear = -1;
    } else {
        rear = rear > 0 ? rear - 1 : sz - 1;
    }
}

template<int sz, typename T>
T& Dequeue<sz, T>::peekFront() {
    if (isEmpty()) {
        throw std::out_of_range("the queue is empty");
    }
    return items[front];
}

template<int sz, typename T>
const T& Dequeue<sz, T>::peekFront() const {
    if (isEmpty()) {
        throw std::out_of_range("the queue is empty");
    }
    return items[front];
}

template<int sz, typename T>
T& Dequeue<sz, T>::peekRear() {
    if (isEmpty()) {
        throw std::out_of_range("the queue is empty");
    }
    return items[rear];
}

template<int sz, typename T>
const T& Dequeue<sz, T>::peekRear() const {
    if (isEmpty()) {
        throw std::out_of_range("the queue is empty");
    }
    return items[rear];
}

template<int sz, typename T>
void Dequeue<sz, T>::clear() {
    front = rear = -1;
}

template<int sz, typename T>
bool Dequeue<sz, T>::isEmpty() const {
    return front < 0 || rear < 0;
}

template<int sz, typename T>
bool Dequeue<sz, T>::isFull() const {
    if (front == 0 && rear + 1 >= sz) { // a cycle (rear + 1) % sz == 0
      return true;
    }
    return front == rear + 1; // a next position
}

template<int sz, typename T>
int Dequeue<sz, T>::cap() const {
    return sz;
}

template<int sz, typename T>
int Dequeue<sz, T>::len() const {
    if (front < 0) return 0;
    if (front <= rear) return rear - front + 1;
    return rear + 1 + sz - front;
}

template<int sz, typename T>
std::ostream& operator<<(std::ostream& os, const Dequeue<sz, T>& queue) {
    if (!queue.isEmpty()) {
        int i;
        for (i = queue.front; i != queue.rear; i = (i + 1) % sz) {
            os << queue.items[i] << ' ';
        }
        os << queue.items[i] << ' ';
    }
    return os;
}
}
