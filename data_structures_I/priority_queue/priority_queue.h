#ifndef PRIORITY_QUEUE_H
#define PRIORITY_QUEUE_H

#include "gtest/gtest.h"
#include <stdexcept>
#include <algorithm>
#include <iostream>

namespace ds {
template<int sz, typename T>
class PriorityQueue {
private:
    FRIEND_TEST(PriorityQueueTest, ExtractElement);
    FRIEND_TEST(PriorityQueueTest, PeekElement);
    FRIEND_TEST(PriorityQueueTest, ClearElements);
    FRIEND_TEST(PriorityQueueTest, InsertToFull);
    FRIEND_TEST(PriorityQueueTest, RemoveElement);

    T heap[sz];
    int top{-1};

    // 0, 1, 2
    // 1, 3, 4
    // 2, 5, 6
    // 3, 7, 8
    // 4, 9, 10
    // ...
    void heapify(int i) {
        int size = len();

        int largest = i;
        int left = 2 * i + 1;
        int right = 2 * i + 2;

        if (left < size && heap[left] > heap[largest]) {
            largest = left;
        }
        if (right < size && heap[right] > heap[largest]) {
            largest = right;
        }
        if (largest != i) {
            std::swap(heap[i], heap[largest]);
            heapify(largest);
        }
    }

    void sort_heap() {
        int size = len();
        for (int i = size / 2 - 1; i >= 0; --i) {
            heapify(i);
        }
    }
public:
    void insert(T v) {
        if (isFull()) {
            throw std::out_of_range("the queue is full");
        }
        int size = len();
        heap[++top] = v;

        sort_heap();
    }

    bool remove(T v) { // delete
        if (isEmpty()) {
            throw std::out_of_range("the queue is empty");
        }

        int m = -1;
        for (int i = 0; i <= top; ++i) {
            if (heap[i] == v) {
                m = i;
                break;
            }
        }

        if (m >= 0) {
            std::swap(heap[m], heap[top]);
            --top;
            sort_heap();
            return true;
        } else {
            return false;
        }
    }

    T extract() {
        if (isEmpty()) {
            throw std::out_of_range("the queue is empty");
        }

        std::swap(heap[0], heap[top]);
        --top;
        sort_heap();

        return heap[top + 1]; // an array is fixed
    }

    bool isEmpty() const {
        return top < 0;
    }

    bool isFull() const {
        return top + 1 >= sz;
    }

    T peek() {
        if (isEmpty()) {
            throw std::out_of_range("the queue is empty");
        }
        return heap[0];
    }

    const T& peek() const {
        if (isEmpty()) {
            throw std::out_of_range("the queue is empty");
        }
        return heap[0];
    }

    int cap() const {
        return sz;
    }

    int len() const {
        return top + 1;
    }

    void clear() {
        top = -1;
    }

    // https://en.cppreference.com/w/cpp/language/friend
    // generates a non-template operator<< for this T

    friend std::ostream& operator<<(std::ostream& os, const PriorityQueue& queue) {
        if (!queue.isEmpty()) {
            os << '\n';
            for (int i = 0; i <= queue.top; ++i) {
                int parent = (i - 1) / 2;

                T v = queue.heap[i];
                T p = queue.heap[parent];

                os << '(' << i << ") " << v;
                if (i) {
                    if (v <= p) {
                        os << " <=";
                    } else {
                        os << " [ERROR] >";
                    }
                    os << " (" << parent << ") " << p;
                }
                os << '\n';
            }
        }
        return os;
    }
};
}

#endif // PRIORITY_QUEUE_H
