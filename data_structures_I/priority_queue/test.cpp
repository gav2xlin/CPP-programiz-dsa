#include "priority_queue.h"
#include "gtest/gtest.h"
#include "gmock/gmock-matchers.h"
#include <vector>

namespace ds {
class PriorityQueueTest : public testing::Test {
};

TEST_F(PriorityQueueTest, EmptyQueue) {
    PriorityQueue<7, int> queue; // 2^(h + 1) - 1

    EXPECT_EQ(0, queue.len());
    EXPECT_EQ(7, queue.cap());
    EXPECT_TRUE(queue.isEmpty());
    EXPECT_FALSE(queue.isFull());
}

TEST_F(PriorityQueueTest, ExtractFromEmpty) {
    PriorityQueue<7, int> queue;

    EXPECT_THROW(queue.extract(), std::out_of_range);
}

TEST_F(PriorityQueueTest, PeekFromEmpty) {
    PriorityQueue<7, int> queue;

    EXPECT_THROW(queue.peek(), std::out_of_range);
}

TEST_F(PriorityQueueTest, ExtractElement) {
    PriorityQueue<7, int> queue;

    queue.insert(3);
    queue.insert(4);
    queue.insert(9);
    queue.insert(5);
    queue.insert(2);

    using ::testing::ElementsAreArray;

    EXPECT_EQ(9, queue.extract());
    EXPECT_EQ(4, queue.len());
    EXPECT_EQ(7, queue.cap());
    EXPECT_FALSE(queue.isEmpty());
    EXPECT_FALSE(queue.isFull());
    EXPECT_THAT(std::vector(std::begin(queue.heap), std::begin(queue.heap) + queue.top + 1), ElementsAreArray({5, 3, 4, 2}));
}

TEST_F(PriorityQueueTest, PeekElement) {
    PriorityQueue<7, int> queue;

    queue.insert(3);
    queue.insert(4);
    queue.insert(9);
    queue.insert(5);
    queue.insert(2);

    using ::testing::ElementsAreArray;

    EXPECT_EQ(9, queue.peek());
    EXPECT_EQ(5, queue.len());
    EXPECT_EQ(7, queue.cap());
    EXPECT_FALSE(queue.isEmpty());
    EXPECT_FALSE(queue.isFull());
    EXPECT_THAT(std::vector(std::begin(queue.heap), std::begin(queue.heap) + queue.top + 1), ElementsAreArray({9, 5, 4, 3, 2}));
}

TEST_F(PriorityQueueTest, ClearElements) {
    PriorityQueue<7, int> queue;

    int v[]{9, 5, 4, 3, 2};
    std::copy(std::begin(v), std::end(v), std::begin(queue.heap));
    queue.top = 4;

    queue.clear();

    EXPECT_EQ(0, queue.len());
    EXPECT_EQ(7, queue.cap());
    EXPECT_TRUE(queue.isEmpty());
    EXPECT_FALSE(queue.isFull());
}

TEST_F(PriorityQueueTest, InsertToFull) {
    PriorityQueue<7, int> queue;
    queue.top = 7;

    EXPECT_FALSE(queue.isEmpty());
    EXPECT_TRUE(queue.isFull());
    EXPECT_THROW(queue.insert(0), std::out_of_range);
}

TEST_F(PriorityQueueTest, RemoveElement) {
    PriorityQueue<7, int> queue;

    queue.insert(3);
    queue.insert(4);
    queue.insert(9);
    queue.insert(5);
    queue.insert(2);

    queue.remove(4);

    using ::testing::ElementsAreArray;

    EXPECT_EQ(4, queue.len());
    EXPECT_EQ(7, queue.cap());
    EXPECT_FALSE(queue.isEmpty());
    EXPECT_FALSE(queue.isFull());
    EXPECT_THAT(std::vector(std::begin(queue.heap), std::begin(queue.heap) + queue.top + 1), ElementsAreArray({9, 5, 2, 3}));
}
}
