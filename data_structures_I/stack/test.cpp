#include "stack.h"
#include "stack.cpp"
#include "gtest/gtest.h"
#include "gmock/gmock-matchers.h"
#include <vector>

namespace ds {
class StackTest : public testing::Test {
};

TEST_F(StackTest, EmptyStack) {
    Stack<10, int> stack;

    EXPECT_EQ(0, stack.len());
    EXPECT_EQ(10, stack.cap());
    EXPECT_TRUE(stack.isEmpty());
    EXPECT_FALSE(stack.isFull());
}

TEST_F(StackTest, PushFirstElements) {
    Stack<10, int> stack;

    for (int i = 1; i < 6; ++i) {
        stack.push(i);
    }

    using ::testing::ElementsAreArray;

    EXPECT_EQ(5, stack.len());
    EXPECT_EQ(10, stack.cap());
    EXPECT_FALSE(stack.isEmpty());
    EXPECT_FALSE(stack.isFull());
    EXPECT_THAT(std::vector(std::begin(stack.items), std::begin(stack.items) + stack.top + 1), ElementsAreArray({1, 2, 3, 4, 5}));
}

TEST_F(StackTest, PushSecondElements) {
    Stack<10, int> stack;

    int v[]{1, 2, 3, 4, 5};
    std::copy(std::begin(v), std::end(v), std::begin(stack.items));
    stack.top = 4;

    for (int i = 6; i <= 10; ++i) {
        stack.push(i);
    }

    using ::testing::ElementsAreArray;

    EXPECT_EQ(10, stack.len());
    EXPECT_EQ(10, stack.cap());
    EXPECT_FALSE(stack.isEmpty());
    EXPECT_TRUE(stack.isFull());
    EXPECT_THAT(std::vector(std::begin(stack.items), std::end(stack.items)), ElementsAreArray({1, 2, 3, 4, 5, 6, 7, 8, 9, 10}));
}

TEST_F(StackTest, PushToFull) {
    Stack<10, int> stack;

    stack.top = 9;

    EXPECT_THROW(stack.push(10), std::out_of_range);
}

TEST_F(StackTest, PopFromEmpty) {
    Stack<10, int> stack;

    EXPECT_THROW(stack.pop(), std::out_of_range);
}

TEST_F(StackTest, ClearElements) {
    Stack<10, int> stack;

    int v[]{1, 2, 3, 4, 5};
    std::copy(std::begin(v), std::end(v), std::begin(stack.items));
    stack.top = 4;

    stack.clear();

    EXPECT_EQ(0, stack.len());
    EXPECT_EQ(10, stack.cap());
    EXPECT_TRUE(stack.isEmpty());
    EXPECT_FALSE(stack.isFull());
}

TEST_F(StackTest, PeekElement) {
    Stack<10, int> stack;

    stack.push(3);
    stack.push(2);
    stack.push(1);

    EXPECT_EQ(1, stack.peek());
}

TEST_F(StackTest, PopElements) {
    Stack<10, int> stack;

    int v[]{1, 2, 3, 4, 5};
    std::copy(std::begin(v), std::end(v), std::begin(stack.items));
    stack.top = 4;

    int res[]{stack.pop(), stack.pop()};

    using ::testing::ElementsAreArray;

    EXPECT_EQ(3, stack.len());
    EXPECT_EQ(10, stack.cap());
    EXPECT_FALSE(stack.isEmpty());
    EXPECT_FALSE(stack.isFull());
    EXPECT_THAT(std::vector(std::begin(res), std::end(res)), ElementsAreArray({5, 4}));
    EXPECT_THAT(std::vector(std::begin(stack.items), std::begin(stack.items) + stack.top + 1), ElementsAreArray({1, 2, 3}));
}
}
