#ifndef STACK_H
#define STACK_H

#include "gtest/gtest.h"
#include <iostream>

namespace ds {
template<int sz, typename T>
class Stack {
private:
    FRIEND_TEST(StackTest, PushFirstElements);
    FRIEND_TEST(StackTest, PushSecondElements);
    FRIEND_TEST(StackTest, PushToFull);
    FRIEND_TEST(StackTest, ClearElements);
    FRIEND_TEST(StackTest, PopElements);

    T items[sz];
    int top{-1};
public:
    void push(T v);
    T pop();
    bool isEmpty() const;
    bool isFull() const;
    T& peek();
    const T& peek() const;
    int cap() const;
    int len() const;
    void clear();

    // https://en.cppreference.com/w/cpp/language/friend Template friend operators

    // refers to a full specialization for this particular T
    // note: this relies on template argument deduction in declarations
    // can also specify the template argument with operator<< <T>"

    friend std::ostream& operator<<(std::ostream& os, const Stack& stack);
};
}

#endif // STACK_H
