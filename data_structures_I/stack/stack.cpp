#include "stack.h"
#include <stdexcept>

namespace ds {
template<int sz, typename T>
void Stack<sz, T>::push(T v) {
    if (isFull()) throw std::out_of_range("the stack is full");
    items[++top] = v;
}

template<int sz, typename T>
T Stack<sz, T>::pop() {
    if (isEmpty()) throw std::out_of_range("the stack is empty");
    return items[top--];
}

template<int sz, typename T>
bool Stack<sz, T>::isEmpty() const {
    return top < 0;
}

template<int sz, typename T>
bool Stack<sz, T>::isFull() const {
    return top + 1 >= sz;
}

template<int sz, typename T>
T& Stack<sz, T>::peek() {
    if (isEmpty()) throw std::out_of_range("the stack is empty");
    return items[top];
}

template<int sz, typename T>
const T& Stack<sz, T>::peek() const {
    if (isEmpty()) throw std::out_of_range("the stack is empty");
    return items[top];
}

template<int sz, typename T>
int Stack<sz, T>::cap() const {
    return sz;
}

template<int sz, typename T>
int Stack<sz, T>::len() const {
    return top + 1;
}

template<int sz, typename T>
void Stack<sz, T>::clear() {
    top = -1;
}

template<int sz, typename T>
std::ostream& operator<<(std::ostream& os, const Stack<sz, T>& stack) {
    for (int i = 0; i <= stack.top; ++i) {
        os << stack.items[i] << ' ';
    }
    return os;
}
}
