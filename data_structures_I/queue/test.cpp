#include "queue.h"
#include "gtest/gtest.h"
#include "gmock/gmock-matchers.h"
#include <vector>

namespace ds {
class QueueTest : public testing::Test {
};

TEST_F(QueueTest, EmptyQueue) {
    Queue<10, int> queue;

    EXPECT_EQ(0, queue.len());
    EXPECT_EQ(10, queue.cap());
    EXPECT_TRUE(queue.isEmpty());
    EXPECT_FALSE(queue.isFull());
}

TEST_F(QueueTest, EnqueueFirstElements) {
    Queue<10, int> queue;

    for (int i = 1; i < 6; ++i) {
        queue.enqueue(i);
    }

    using ::testing::ElementsAreArray;

    EXPECT_EQ(5, queue.len());
    EXPECT_EQ(10, queue.cap());
    EXPECT_FALSE(queue.isEmpty());
    EXPECT_FALSE(queue.isFull());
    EXPECT_THAT(std::vector(std::begin(queue.items), std::begin(queue.items) + queue.rear + 1), ElementsAreArray({1, 2, 3, 4, 5}));
}

TEST_F(QueueTest, EnqueueSecondElements) {
    Queue<10, int> queue;

    int v[]{1, 2, 3, 4, 5};
    std::copy(std::begin(v), std::end(v), std::begin(queue.items));
    queue.front = 0;
    queue.rear = 4;

    for (int i = 6; i <= 10; ++i) {
        queue.enqueue(i);
    }

    using ::testing::ElementsAreArray;

    EXPECT_EQ(10, queue.len());
    EXPECT_EQ(10, queue.cap());
    EXPECT_FALSE(queue.isEmpty());
    EXPECT_TRUE(queue.isFull());
    EXPECT_THAT(std::vector(std::begin(queue.items), std::end(queue.items)), ElementsAreArray({1, 2, 3, 4, 5, 6, 7, 8, 9, 10}));
}

TEST_F(QueueTest, EnqueueToFull) {
    Queue<10, int> queue;

    queue.front = 0;
    queue.rear = 9;

    EXPECT_THROW(queue.enqueue(10), std::out_of_range);
}

TEST_F(QueueTest, DequeueFromEmpty) {
    Queue<10, int> queue;

    EXPECT_THROW(queue.dequeue(), std::out_of_range);
}

TEST_F(QueueTest, ClearElements) {
    Queue<10, int> queue;

    int v[]{1, 2, 3, 4, 5};
    std::copy(std::begin(v), std::end(v), std::begin(queue.items));
    queue.front = 0;
    queue.rear = 4;

    queue.clear();

    EXPECT_EQ(0, queue.len());
    EXPECT_EQ(10, queue.cap());
    EXPECT_TRUE(queue.isEmpty());
    EXPECT_FALSE(queue.isFull());
}

TEST_F(QueueTest, PeekElement) {
    Queue<10, int> queue;

    int v[]{1, 2, 3, 4, 5};
    std::copy(std::begin(v), std::end(v), std::begin(queue.items));
    queue.front = 0;
    queue.rear = 4;

    for (int i = 0; i < 3; ++i) {
        queue.dequeue();
    }

    EXPECT_EQ(4, queue.peek());
}

TEST_F(QueueTest, DequeueElements) {
    Queue<10, int> queue;

    int v[]{1, 2, 3, 4, 5};
    std::copy(std::begin(v), std::end(v), std::begin(queue.items));
    queue.front = 0;
    queue.rear = 4;

    int res[]{queue.dequeue(), queue.dequeue()};

    using ::testing::ElementsAreArray;

    EXPECT_EQ(3, queue.len());
    EXPECT_EQ(10, queue.cap());
    EXPECT_FALSE(queue.isEmpty());
    EXPECT_FALSE(queue.isFull());
    EXPECT_THAT(std::vector(std::begin(res), std::end(res)), ElementsAreArray({1, 2}));
    EXPECT_THAT(std::vector(std::begin(queue.items) + queue.front, std::begin(queue.items) + queue.rear + 1), ElementsAreArray({3, 4, 5}));
}
}
