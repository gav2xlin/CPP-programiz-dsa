#ifndef QUEUE_H
#define QUEUE_H

#include "gtest/gtest.h"
#include <stdexcept>
#include <iostream>

namespace ds {
template<int sz, typename T>
class Queue {
private:
    FRIEND_TEST(QueueTest, EnqueueFirstElements);
    FRIEND_TEST(QueueTest, EnqueueSecondElements);
    FRIEND_TEST(QueueTest, EnqueueToFull);
    FRIEND_TEST(QueueTest, ClearElements);
    FRIEND_TEST(QueueTest, PeekElement);
    FRIEND_TEST(QueueTest, DequeueElements);

    T items[sz];
    int front{-1}, rear{-1};
public:
    void enqueue(T v) {
        if (isFull()) {
            throw std::out_of_range("the queue is full");
        }
        if (front < 0) front = 0;
        items[++rear] = v;
    }

    T dequeue() {
        if (isEmpty() || front > rear) {
            throw std::out_of_range("the queue is empty");
        }
        T v = items[front++];
        return v;
    }

    bool isEmpty() const {
        return front < 0;
    }

    bool isFull() const {
        return rear + 1 >= sz;
    }

    T& peek() {
        if (isEmpty()) {
            throw std::out_of_range("the queue is empty");
        }
        return items[front];
    }

    const T& peek() const {
        if (isEmpty()) {
            throw std::out_of_range("the queue is empty");
        }
        return items[front];
    }

    int cap() const {
        return sz;
    }

    int len() const {
        return front >= 0 ? rear - front + 1 : 0;
    }

    void clear() {
        front = rear = -1;
    }

    // https://en.cppreference.com/w/cpp/language/friend
    // generates a non-template operator<< for this T

    friend std::ostream& operator<<(std::ostream& os, const Queue& queue) {
        if (!queue.isEmpty()) {
            for (int i = queue.front; i <= queue.rear; ++i) {
                os << queue.items[i] << ' ';
            }
        }
        return os;
    }
};
}

#endif // QUEUE_H
