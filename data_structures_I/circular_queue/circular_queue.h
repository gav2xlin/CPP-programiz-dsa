#ifndef CIRCULAR_QUEUE_H
#define CIRCULAR_QUEUE_H

#include "gtest/gtest.h"
#include <stdexcept>
#include <iostream>

namespace ds {
template<int sz, typename T>
class CircularQueue {
private:
    FRIEND_TEST(CircularQueueTest, EnqueueFirstElements);
    FRIEND_TEST(CircularQueueTest, EnqueueSecondElements);
    FRIEND_TEST(CircularQueueTest, CircularEnqueue);
    FRIEND_TEST(CircularQueueTest, EnqueueToFull);
    FRIEND_TEST(CircularQueueTest, IsQueueFull);
    FRIEND_TEST(CircularQueueTest, ClearElements);
    FRIEND_TEST(CircularQueueTest, PeekElement);
    FRIEND_TEST(CircularQueueTest, DequeueElements);
    FRIEND_TEST(CircularQueueTest, CircularDequeue);

    T items[sz];
    int front{-1}, rear{-1};
public:
    void enqueue(T v) {
        if (isFull()) {
            throw std::out_of_range("the queue is full");
        }
        if (front < 0) {
            front = 0;
        }
        rear = (rear + 1) % sz;
        items[rear] = v;
    }

    T dequeue() {
        if (isEmpty()) {
            throw std::out_of_range("the queue is empty");
        }
        T v = items[front];
        if (front == rear) {
            front = rear = -1;
        } else {
            front = (front + 1) % sz;
        }
        return v;
    }

    bool isEmpty() const {
        return front < 0;
    }

    bool isFull() const {
        if (front == 0 && rear + 1 >= sz) { // a cycle (rear + 1) % sz == 0
          return true;
        }
        return front == rear + 1; // a next position
    }

    T& peek() {
        if (isEmpty()) {
            throw std::out_of_range("the queue is empty");
        }
        return items[front];
    }

    const T& peek() const {
        if (isEmpty()) {
            throw std::out_of_range("the queue is empty");
        }
        return items[front];
    }

    int cap() const {
        return sz;
    }

    int len() const {
        if (front < 0) return 0;
        if (front <= rear) return rear - front + 1;
        return rear + 1 + sz - front;
    }

    void clear() {
        front = rear = -1;
    }

    // https://en.cppreference.com/w/cpp/language/friend
    // generates a non-template operator<< for this T

    friend std::ostream& operator<<(std::ostream& os, const CircularQueue& queue) {
        if (!queue.isEmpty()) {
            int i;
            for (i = queue.front; i != queue.rear; i = (i + 1) % sz) {
                os << queue.items[i] << ' ';
            }
            os << queue.items[i] << ' ';
        }
        return os;
    }
};
}

#endif // CIRCULAR_QUEUE_H
