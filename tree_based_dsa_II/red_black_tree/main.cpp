#include "red_black_tree.h"
#include <iostream>

using namespace std;
using namespace ds;

int main()
{
    RedBlackTree<int> tree;
    tree.insertNode(55);
    tree.insertNode(40);
    tree.insertNode(65);
    tree.insertNode(60);
    tree.insertNode(75);
    tree.insertNode(57);

    cout << tree << endl;

    tree.deleteNode(40);

    cout << "After deleting: 40 " << endl;
    cout << tree << endl;

    return 0;
}
