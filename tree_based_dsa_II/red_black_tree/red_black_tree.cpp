#include "red_black_tree.h"
#include <sstream>
#include <stdexcept>
#include <algorithm>

namespace ds {
// private methods
template<typename T>
void RedBlackTree<T>::deleteAll(Node *node) {
    if (node != nullptr) {
        if (node->left != nullptr) {
            deleteAll(node->left);
            node->left = nullptr;
        }

        if (node->right != nullptr) {
            deleteAll(node->right);
            node->right = nullptr;
        }

        delete node;
    }
}

template <> std::unique_ptr<ds::RedBlackTree<int>::Node> ds::RedBlackTree<int>::NIL = std::make_unique<ds::RedBlackTree<int>::Node>();

template<typename T>
bool RedBlackTree<T>::_hasNode(Node* node, T key) {
    if (node != nullptr && node != NIL.get()) {
        if (node->data == key) return true;

        if (key < node->data) {
            return _hasNode(node->left, key);
        } else if (key > node->data) {
            return _hasNode(node->right, key);
        }
    }
    return false;
}

template<typename T>
void RedBlackTree<T>::printTree(std::ostream& os, Node *node, std::string indent, bool rln) {
    if (node != nullptr) {
        os << indent;

        if (rln) {
            os << "R----";
            indent += "   ";
        } else {
            os << "L----";
            indent += "|  ";
        }

        std::string color = node->color == NodeColors::red ? "RED" : "BLACK";
        os << node->data << "(" << color << ")" << std::endl;

        printTree(os, node->left, indent, false);
        printTree(os, node->right, indent, true);
    }
}

template<typename T>
void RedBlackTree<T>::leftRotate(Node* x) {
    Node* y = x->right;

    x->right = y->left;
    if (y->left != NIL.get()) {
        y->left->parent = x;
    }

    y->parent = x->parent;
    if (x->parent == nullptr) {
        this->root = y;
    } else if (x == x->parent->left) {
        x->parent->left = y;
    } else {
        x->parent->right = y;
    }

    y->left = x;
    x->parent = y;
}

template<typename T>
void RedBlackTree<T>::rightRotate(Node* x) {
    Node* y = x->left;

    x->left = y->right;
    if (y->right != NIL.get()) {
        y->right->parent = x;
    }

    y->parent = x->parent;
    if (x->parent == nullptr) {
        this->root = y;
    } else if (x == x->parent->right) {
        x->parent->right = y;
    } else {
        x->parent->left = y;
    }

    y->right = x;
    x->parent = y;
}

template<typename T>
void RedBlackTree<T>::insertFix(Node* k) {
    Node* u;

    while (k->parent->color == NodeColors::red) {
        if (k->parent == k->parent->parent->right) {
            u = k->parent->parent->left;

            if (u->color == NodeColors::red) {
                u->color = NodeColors::black;
                k->parent->color = NodeColors::black;
                k->parent->parent->color = NodeColors::red;

                k = k->parent->parent;
            } else {
                if (k == k->parent->left) {
                    k = k->parent;
                    rightRotate(k);
                }

                k->parent->color = NodeColors::black;
                k->parent->parent->color = NodeColors::red;
                leftRotate(k->parent->parent);
            }
        } else {
            u = k->parent->parent->right;

            if (u->color == NodeColors::red) {
                u->color = NodeColors::black;
                k->parent->color = NodeColors::black;
                k->parent->parent->color = NodeColors::red;

                k = k->parent->parent;
            } else {
                if (k == k->parent->right) {
                    k = k->parent;
                    leftRotate(k);
                }

                k->parent->color = NodeColors::black;
                k->parent->parent->color = NodeColors::red;
                rightRotate(k->parent->parent);
            }
        }

        if (k == root) {
            break;
        }
    }

    root->color = NodeColors::black;
}

template<typename T>
void RedBlackTree<T>::rbTransplant(Node* u, Node* v) {
    if (u->parent == nullptr) {
        root = v;
    } else if (u == u->parent->left) {
        u->parent->left = v;
    } else {
        u->parent->right = v;
    }
    v->parent = u->parent;
}

template<typename T>
typename RedBlackTree<T>::Node* RedBlackTree<T>::minimum(Node* node) {
    while (node->left != NIL.get()) {
        node = node->left;
    }
    return node;
}

template<typename T>
typename RedBlackTree<T>::Node* RedBlackTree<T>::maximum(Node* node) {
    while (node->right != NIL.get()) {
        node = node->right;
    }
    return node;
}

template<typename T>
typename RedBlackTree<T>::Node* RedBlackTree<T>::successor(Node* x) {
    if (x->right != NIL.get()) {
        return minimum(x->right);
    }

    Node* y = x->parent;
    while (y != NIL.get() && x == y->right) {
        x = y;
        y = y->parent;
    }
    return y;
}

template<typename T>
typename RedBlackTree<T>::Node* RedBlackTree<T>::predecessor(Node* x) {
    if (x->left != NIL.get()) {
        return maximum(x->left);
    }

    Node* y = x->parent;
    while (y != NIL.get() && x == y->left) {
        x = y;
        y = y->parent;
    }

    return y;
}

template<typename T>
void RedBlackTree<T>::deleteFix(Node* x) {
    Node* s;

    while (x != root && x->color == NodeColors::black) {
        if (x == x->parent->left) {
            s = x->parent->right;

            if (s->color == NodeColors::red) {
                s->color = NodeColors::black;
                x->parent->color = NodeColors::red;
                leftRotate(x->parent);
                s = x->parent->right;
            }

            if (s->left->color == NodeColors::black && s->right->color == NodeColors::black) {
                s->color = NodeColors::red;
                x = x->parent;
            } else {
                if (s->right->color == NodeColors::black) {
                    s->left->color = NodeColors::black;
                    s->color = NodeColors::red;
                    rightRotate(s);
                    s = x->parent->right;
                }

                s->color = x->parent->color;
                x->parent->color = NodeColors::black;
                s->right->color = NodeColors::black;
                leftRotate(x->parent);
                x = root;
            }
        } else {
            s = x->parent->left;

            if (s->color == NodeColors::red) {
                s->color = NodeColors::black;
                x->parent->color = NodeColors::red;
                rightRotate(x->parent);
                s = x->parent->left;
            }

            if (s->right->color == NodeColors::black && s->right->color == NodeColors::black) {
                s->color = NodeColors::red;
                x = x->parent;
            } else {
                if (s->left->color == NodeColors::black) {
                    s->right->color = NodeColors::black;
                    s->color = NodeColors::red;
                    leftRotate(s);
                    s = x->parent->left;
                }

                s->color = x->parent->color;
                x->parent->color = NodeColors::black;
                s->left->color = NodeColors::black;
                rightRotate(x->parent);
                x = root;
            }
        }
    }

    x->color = NodeColors::black;
}

template<typename T>
void RedBlackTree<T>::_deleteNode(Node* node, T key) {
    Node* z = NIL.get();
    Node *x, *y;

    while (node != NIL.get()) {
        if (node->data == key) {
            z = node;
        }

        if (node->data <= key) {
            node = node->right;
        } else {
            node = node->left;
        }
    }

    if (z == NIL.get()) {
        std::cout << "Key not found in the tree" << std::endl;
        return;
    }

    y = z;
    NodeColors y_original_color = y->color;

    if (z->left == NIL.get()) {
        x = z->right;
        rbTransplant(z, z->right);
    } else if (z->right == NIL.get()) {
        x = z->left;
        rbTransplant(z, z->left);
    } else {
        y = minimum(z->right);
        y_original_color = y->color;
        x = y->right;

        if (y->parent == z) {
            x->parent = y;
        } else {
            rbTransplant(y, y->right);
            y->right = z->right;
            y->right->parent = y;
        }

        rbTransplant(z, y);
        y->left = z->left;
        y->left->parent = y;
        y->color = z->color;
    }

    delete z;

    if (y_original_color == NodeColors::black) {
        deleteFix(x);
    }
}

// public methods
template<typename T>
RedBlackTree<T>::RedBlackTree(): root(NIL.get())
{
}

template<typename T>
RedBlackTree<T>::~RedBlackTree()
{
    if (root != nullptr) {
        deleteAll(root->left);
        deleteAll(root->right);

        delete root;
    }
}

template<typename T>
void RedBlackTree<T>::insertNode(T key) {
    Node* node = new Node{.data = key, .parent = nullptr, .left = NIL.get(), .right = NIL.get(), .color = NodeColors::red};

    Node* y = nullptr;
    Node* x = this->root;

    while (x != NIL.get()) {
        y = x;
        if (node->data < x->data) {
            x = x->left;
        } else {
            x = x->right;
        }
    }

    node->parent = y;
    if (y == nullptr) {
        root = node;
    } else if (node->data < y->data) {
        y->left = node;
    } else {
        y->right = node;
    }

    if (node->parent == nullptr) {
        node->color = NodeColors::black;
        return;
    }

    if (node->parent->parent == nullptr) {
        return;
    }

    insertFix(node);
}

/***/

template<typename T>
typename RedBlackTree<T>::Node * RedBlackTree<T>::grandparent(Node *n) {
    if (n != nullptr && n->parent != nullptr) {
        return n->parent->parent;
    } else {
        return nullptr;
    }
}

template<typename T>
typename RedBlackTree<T>::Node * RedBlackTree<T>::uncle(Node *n) {
    Node *g = grandparent(n);
    if (g == nullptr) {
        return nullptr; // No grandparent means no uncle
    }
    if (n->parent == g->left) {
        return g->right;
    } else {
        return g->left;
    }
}

template<typename T>
void RedBlackTree<T>::insert_case1(Node *n) {
    if (n->parent == nullptr) {
        // root(black)
        n->color = NodeColors::black;
    } else {
        insert_case2(n);
    }
}

template<typename T>
void RedBlackTree<T>::insert_case2(Node *n) {
    if (n->parent->color == NodeColors::black) {
        //     parent(black)
        // node(red) ...
        return; /* Tree is still valid */
    } else {
        insert_case3(n);
    }
}

template<typename T>
void RedBlackTree<T>::insert_case3(Node *n) {
    Node *u = uncle(n);

    //     grandparent(black)
    //   parent(red) + uncle(red)
    // node(red) + ...
    if (u != nullptr && u->color == NodeColors::red && n->parent->color == NodeColors::red) {
        n->parent->color = NodeColors::black;
        u->color = NodeColors::black;

        // grandparent(red)
        Node* g = grandparent(n);
        g->color = NodeColors::red;

        //     grandparent(red)
        //   parent(black) + uncle(black)
        // node(red) + ...

        insert_case1(g); // grandparent(red) -> root(black) - red may be turned into black
    } else {
        insert_case4(n);
    }
}

template<typename T>
void RedBlackTree<T>::insert_case4(Node *n) {
    Node *g = grandparent(n);

    if (n == n->parent->right && n->parent == g->left) {
        //     grandparent(black)
        //   parent(red) + uncle(black)
        // ... + node(red)

        /* rotate left:
         * g = grandparent(n)
         * Node *saved_p=g->left, *saved_left_n=n->left;
         * g->left=n;
         * n->left=saved_p;
         * saved_p->right=saved_left_n;
        */

        //         grandparent(black)
        //     node(red) + uncle(black)
        // parent(red) + ...

        leftRotate(n->parent);
        n = n->left;
    } else if (n == n->parent->left && n->parent == g->right) {
        //     grandparent(black)
        //   uncle(black) + parent(red)
        //                node(red) + ...

        /* rotate right:
         * g = grandparent(n)
         * Node *saved_p=g->right, *saved_right_n=n->right;
         * g->right=n;
         * n->right=saved_p;
         * saved_p->left=saved_right_n;
        */
        //     grandparent(black)
        //   uncle(black) + node(red)
        //                ... + parent(red)

        rightRotate(n->parent);
        n = n->right;
    }

    insert_case5(n);
}

template<typename T>
void RedBlackTree<T>::insert_case5(Node *n) {
    Node *g = grandparent(n);

    n->parent->color = NodeColors::black;
    g->color = NodeColors::red;

    if (n == n->parent->left && n->parent == g->left) {
        //         grandparent(black)
        //    parent(red) + uncle(black)
        // node(red) + ...

        //           parent(black)
        //     node(red) + grandparent(red)
        //                ... + uncle(black)

        rightRotate(g);
    } else {
        /* (n == n->parent->right) && (n->parent == g->right) */
        //         grandparent(black)
        //    uncle(black) + parent(red)
        //                 ... + node(red)

        //           parent(black)
        //    grandparent(black) + node(red)
        //  uncle(black) + ...

        leftRotate(g);
    }
}

/***/

template<typename T>
typename RedBlackTree<T>::Node * RedBlackTree<T>::sibling(Node *n) {
    if (n == n->parent->left) {
        return n->parent->right;
    } else {
        return n->parent->left;
    }
}

template<typename T>
bool RedBlackTree<T>::is_leaf(Node *n) {
    return n != nullptr && n != NIL.get();
}

template<typename T>
void RedBlackTree<T>::replace_node(Node *n, Node* child) {
    if (n->parent->left == n) {
        n->parent->left = child;
    } else {
        // (n->parent->right == n)
        n->parent->right = child;
    }

    child->left = n->left;
    child->right = n->right;
}

template<typename T>
void RedBlackTree<T>::delete_one_child(Node *n) {
    Node *child = is_leaf(n->right) ? n->left : n->right;

    replace_node(n, child);

    if (n->color == NodeColors::black) {
        if (child->color == NodeColors::red) {
            child->color = NodeColors::black;
        } else {
            delete_case1(child);
        }
    }
    delete n;
}

template<typename T>
void RedBlackTree<T>::delete_case1(Node *n) {
    if (n->parent != nullptr) {
        delete_case2(n);
    }
}

template<typename T>
void RedBlackTree<T>::delete_case2(Node *n) {
    Node *s = sibling(n);

    if (s->color == NodeColors::red) {
        n->parent->color = NodeColors::red;
        s->color = NodeColors::black;

        if (n == n->parent->left) {
            //           parent(black)
            //    node(black) + sibling(red)
            //             SL(black) + SR(black)

            //           sibling(black)
            //       parent(red) + SR(black)
            // node(black) + SL(black)

            leftRotate(n->parent);
        } else {
            // (n == n->parent->right)

            //           parent(black)
            //     sibling(red) + node(black)
            // SL(black) + SR(black)

            //           sibling(black)
            //       SL(black) + parent(red)
            //               SR(black) + node(black)

            rightRotate(n->parent);
        }
    }

    delete_case3(n);
}

template<typename T>
void RedBlackTree<T>::delete_case3(Node *n) {
    Node *s = sibling(n);

    //           parent(black)
    //    node(black) + sibling(black)
    //             SL(black) + SR(black)

    if (n->parent->color == NodeColors::black && s->color == NodeColors::black &&
        s->left->color == NodeColors::black && s->right->color == NodeColors::black) {

        //           parent(black)
        //    node(black) + sibling(red)
        //             SL(black) + SR(black)

        s->color = NodeColors::red;

        delete_case1(n->parent);
    } else {
        delete_case4(n);
    }
}

template<typename T>
void RedBlackTree<T>::delete_case4(Node *n) {
    Node *s = sibling(n);

    //           parent(red)
    //    node(black) + sibling(black)
    //             SL(black) + SR(black)

    if (n->parent->color == NodeColors::red && s->color == NodeColors::black &&
        s->left->color == NodeColors::black && s->right->color == NodeColors::black) {

        //           parent(black)
        //    node(black) + sibling(red)
        //             SL(black) + SR(black)

        s->color = NodeColors::red;
        n->parent->color = NodeColors::black;
    } else {
        delete_case5(n);
    }
}

template<typename T>
void RedBlackTree<T>::delete_case5(Node *n) {
    Node *s = sibling(n);

    if (s->color == NodeColors::black) {
        /* this if statement is trivial,
         * due to case 2 (even though case 2 changed the sibling to a sibling's child,
         * the sibling's child can't be red, since no red parent can have a red child). */

        /* the following statements just force the red to be on the left of the left of the parent,
         * or right of the right, so case six will rotate correctly. */

        if (n == n->parent->left && s->right->color == NodeColors::black && s->left->color == NodeColors::red) {
            /* this last test is trivial too due to cases 2-4. */

            //      sigling(black)
            //    SL(red) + SR(black)

            s->color = NodeColors::red;
            s->left->color = NodeColors::black;

            //       SL(black)
            //     ... + sibling(red)
            //           ... + SR(black)

            rightRotate(s);
        } else if (n == n->parent->right && s->left->color == NodeColors::black && s->right->color == NodeColors::red) {
            /* this last test is trivial too due to cases 2-4. */

            //      sigling(black)
            //    SL(black) + SR(red)

            s->color = NodeColors::red;
            s->right->color = NodeColors::black;

            //        SR(black)
            //     sigling(red) + ...
            //   SL(black) + ...

            leftRotate(s);
        }
    }

    delete_case6(n);
}

template<typename T>
void RedBlackTree<T>::delete_case6(Node *n) {
    Node *s = sibling(n);

    s->color = n->parent->color;
    n->parent->color = NodeColors::black;

    if (n == n->parent->left) {
        //           parent(...)
        //    node(black) + sibling(black)
        //                      ... + SR(red)

        s->right->color = NodeColors::black;

        //           sibling(...)
        //     parent(black) + SR(black)
        //  node(black) + ...

        leftRotate(n->parent);
    } else {
        //           parent(...)
        //    sibling(black) + node(black)
        //  SL(red) + ...

        s->left->color = NodeColors::black;

        //           sibling(...)
        //    SL(black) + parent(black)
        //                    ... + node(black)

        rightRotate(n->parent);
    }
}

/***/

template<typename T>
bool RedBlackTree<T>::hasNode(T key) {
    return _hasNode(root, key);
}

template<typename T>
void RedBlackTree<T>::deleteNode(T key) {
    _deleteNode(root, key);
}
}

template class ds::RedBlackTree<int>;
