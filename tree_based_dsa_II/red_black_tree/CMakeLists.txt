cmake_minimum_required(VERSION 3.5)

project(red_black_tree LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include(FetchContent)
FetchContent_Declare(
  googletest
  GIT_REPOSITORY https://github.com/google/googletest.git
  GIT_TAG        release-1.12.1
)
# For Windows: Prevent overriding the parent project's compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

set(RED_BLACK_TREE_SOURCES red_black_tree.cpp)
set(RED_BLACK_TREE_HEADERS red_black_tree.h)

add_executable(red_black_tree_test ${RED_BLACK_TREE_SOURCES} ${RED_BLACK_TREE_HEADERS} test.cpp)
target_link_libraries(red_black_tree_test gtest_main gmock)
add_test(NAME red_black_tree_test COMMAND red_black_tree_test)

add_executable(red_black_tree ${RED_BLACK_TREE_SOURCES} ${RED_BLACK_TREE_HEADERS} main.cpp)
target_link_libraries(red_black_tree gtest gmock)
