#ifndef RED_BLACK_TREE_H
#define RED_BLACK_TREE_H

#include <iostream>
#include <string>
#include <memory>

namespace ds {
template<typename T>
class RedBlackTree {
private:
    enum class NodeColors { red, black };

    struct Node {
        T data;
        Node *parent{nullptr}, *left{nullptr}, *right{nullptr};
        NodeColors color{NodeColors::black};
    };

    static std::unique_ptr<Node> NIL;
    Node* root{nullptr};

    void deleteAll(Node *node);
    bool _hasNode(Node* node, T key);

    static void printTree(std::ostream& os, Node *node, std::string indent, bool rln);

    void leftRotate(Node *x);
    void rightRotate(Node *y);

    void rbTransplant(Node* u, Node* v);
    Node* minimum(Node* node);
    Node* maximum(Node* node);
    Node* successor(Node* x);
    Node* predecessor(Node* x);

    void insertFix(Node* k);
    void _deleteNode(Node* node, T key);
    void deleteFix(Node* x);

    /***/
    Node* grandparent(Node *n);
    Node* uncle(Node* n);

    void insert_case1(Node *n);
    void insert_case2(Node *n);
    void insert_case3(Node *n);
    void insert_case4(Node *n);
    void insert_case5(Node *n);
    /***/

    /***/
    Node * sibling(Node *n);
    bool is_leaf(Node *n);
    void replace_node(Node *n, Node* child);

    void delete_one_child(Node *n);
    void delete_case1(Node *n);
    void delete_case2(Node *n);
    void delete_case3(Node *n);
    void delete_case4(Node *n);
    void delete_case5(Node *n);
    void delete_case6(Node *n);
    /***/
public:
    RedBlackTree();
    ~RedBlackTree();

    RedBlackTree(const RedBlackTree &other) = delete; // copy constructor
    RedBlackTree& operator=(const RedBlackTree& other) = delete; // copy assignment operator

    RedBlackTree(RedBlackTree&& other) = delete; // move constructor
    RedBlackTree& operator=(RedBlackTree&& other) = delete; // move assignment operator

    void insertNode(T key);
    bool hasNode(T key);
    void deleteNode(T key);

    friend std::ostream& operator<<(std::ostream& os, const RedBlackTree<T>& tree) {
        RedBlackTree<T>::printTree(os, tree.root, "", true);
        return os;
    }
};

template<typename T>
typename RedBlackTree<T>::Node* NIL = std::make_unique<T>();
}

/*template<typename T>
class AVLTree
{
private:
    Node* _insertNode(Node* node, T key);

    Node* findLeftMostNode(Node *node);
public:

};
}*/

#endif // RED_BLACK_TREE_H
