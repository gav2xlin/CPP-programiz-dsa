#include "btree.h"
#include <stdexcept>

namespace ds {
template<typename T>
BTree<T>::BTree(int _t) {
    root = nullptr;
    t = _t; // limit
}

template<typename T>
BTree<T>::~BTree() {
    if (root != nullptr) {
        delete root;
    }
}

template<typename T>
void BTree<T>::traverse(std::ostream& os) const {
    if (root != nullptr) {
        os << *root;
    }
}

template<typename T>
void BTree<T>::insert(T k) {
    if (root == nullptr) {
        root = new BTreeNode<T>(t, true);
        root->keys[0] = k;
        root->n = 1;
    } else {
        if (root->n == 2 * t - 1) {
            // create one root element
            BTreeNode<T> *s = new BTreeNode<T>(t, false);

            s->C[0] = root;

            s->splitChild(0, root);

            int i = 0;
            if (s->keys[0] < k) {
                ++i;
            }
            s->C[i]->insertNonFull(k);

            root = s;
        } else {
            root->insertNonFull(k);
        }
    }
}

template<typename T>
void BTree<T>::remove(T k) {
    if (!root) {
        throw std::invalid_argument("The tree is empty");
    }

    root->deletion(k);

    if (root->n == 0) {
        BTreeNode<T> *tmp = root;
        if (root->leaf) {
            root = nullptr;
        } else {
            root = root->C[0];
        }

        delete tmp;
    }
}

template<typename T>
BTreeNode<T>* BTree<T>::search(T k) {
    return (root == nullptr) ? nullptr : root->search(k);
}
}
