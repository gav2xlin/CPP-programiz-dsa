#ifndef BTREE_H
#define BTREE_H

#include "btree_node.h"

namespace ds {
template<typename T>
class BTree {
    BTreeNode<T> *root;
    int t;
public:
    BTree(int _t);
    ~BTree();

    void traverse(std::ostream& os) const;

    void insert(T k);

    void remove(T k);

    BTreeNode<T>* search(T k);

    friend std::ostream& operator<<(std::ostream& os, const BTree& tree) {
        tree.traverse(os);
        return os;
    }
};
}

#endif // BTREE_H
