#ifndef BTREENODE_H
#define BTREENODE_H

#include <iostream>

namespace ds {
template<typename T>
class BTreeNode {
    T *keys;
    int t;
    BTreeNode **C; // children
    int n;
    bool leaf;
public:
    BTreeNode(int _t, bool _leaf);
    ~BTreeNode();

    void traverse(std::ostream& os) const;

    void insertNonFull(T k);
    void splitChild(int idx, BTreeNode *y);

    void deletion(T k);
    int findKey(T k);
    void removeFromLeaf(int idx);
    void removeFromNonLeaf(int idx);
    int getPredecessor(int idx);
    int getSuccessor(int idx);
    void borrowFromPrev(int idx);
    void borrowFromNext(int idx);
    void fill(int idx);
    void merge(int idx);

    BTreeNode *search(T k);

    friend std::ostream& operator<<(std::ostream& os, const BTreeNode& node) {
        node.traverse(os);
        return os;
    }

    template<typename U> friend class BTree;
    // friend class BTree<T>; // recursive
};
}

#endif // BTREENODE_H
