#include "btree.h"
#include "btree.cpp"
#include "btree_node.cpp"
#include <iostream>

using namespace std;
using namespace ds;

int main()
{
    BTree<int> tree(3);
    tree.insert(8);
    tree.insert(9);
    tree.insert(10);
    tree.insert(11);
    tree.insert(15);
    tree.insert(16);
    tree.insert(17);
    tree.insert(18);
    tree.insert(20);
    tree.insert(23);

    cout << "The B-tree is:\n" << tree << endl;

    cout << "20" << (tree.search(20) != nullptr ? " is found" : " is not found") << endl;

    tree.remove(20);

    cout << "\nThe B-tree is:\n" << tree << endl;

    cout << "20" << (tree.search(20) != nullptr ? " is found" : " is not found") << endl;

    return 0;
}
