#ifndef BPTREE_NODE_H
#define BPTREE_NODE_H

namespace ds {
template<typename T>
class BPTReeNode {
    bool leaf;
    T *key;
    int size;
    BPTReeNode **ptr;

    template<typename U> friend class BPTree;
    // friend class BPTree<T>; // recursive
public:
    BPTReeNode();
    ~BPTReeNode();
};
}

#endif // BPTREE_NODE_H
