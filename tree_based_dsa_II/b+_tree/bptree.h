#ifndef BPTREE_H
#define BPTREE_H

#include "bptree_node.h"
#include <iostream>

namespace ds {
template<typename T>
class BPTree {
    BPTReeNode<T> *root;

    void insertInternal(T, BPTReeNode<T> *, BPTReeNode<T> *);
    void removeInternal(T, BPTReeNode<T> *, BPTReeNode<T> *);
    BPTReeNode<T> *findParent(BPTReeNode<T> *, BPTReeNode<T> *);
public:
    BPTree();
    ~BPTree();

    bool search(T);
    void insert(T);
    void remove(T);

    void display(std::ostream& os, const BPTReeNode<T> *) const;

    BPTReeNode<T> *getRoot();
    BPTReeNode<T> *getRoot() const;

    friend std::ostream& operator<<(std::ostream& os, const BPTree& tree) {
        tree.display(os, tree.getRoot());
        return os;
    }
};
}

#endif // BPTREE_H
