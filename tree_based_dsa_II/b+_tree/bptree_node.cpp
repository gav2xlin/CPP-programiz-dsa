#include "bptree_node.h"
#include "const.h"

namespace ds {
template<typename T>
BPTReeNode<T>::BPTReeNode() {
    key = new T[MAX];
    ptr = new BPTReeNode* [MAX + 1]{nullptr};
}

template<typename T>
BPTReeNode<T>::~BPTReeNode() {
    delete[] key;
    for (int i = 0; i <= MAX; ++i) {
        delete ptr[i];
    }
    delete[] ptr;
}
}
