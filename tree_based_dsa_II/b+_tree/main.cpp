#include "bptree.h"
#include "bptree.cpp"
#include "bptree_node.cpp"
#include <iostream>

using namespace std;
using namespace ds;

int main()
{
    BPTree<int> tree;
    tree.insert(5);
    tree.insert(15);
    tree.insert(25);
    tree.insert(35);
    tree.insert(45);

    cout << "The B+ tree is:\n" << tree << endl;

    cout << "15" << (tree.search(15) ? " is found" : " is not found") << endl;

    tree.remove(15);

    cout << "\nThe B+ tree is:\n" << tree << endl;

    cout << "15" << (tree.search(15) ? " is found" : " is not found") << endl;

    return 0;
}
