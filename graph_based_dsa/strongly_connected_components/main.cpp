#include "graph.h"
#include <iostream>

using namespace std;

int main()
{
    Graph g(8);
    g.addEdge(0, 1);
    g.addEdge(1, 2);
    g.addEdge(2, 3);
    g.addEdge(2, 4);
    g.addEdge(3, 0);
    g.addEdge(4, 5);
    g.addEdge(5, 6);
    g.addEdge(6, 4);
    g.addEdge(6, 7);

    cout << "Strongly Connected Components:\n";
    g.printSCC();

    // stack = 3, 7, 6, 5, 4, 2, 1, 0

    // scc0 = 0 3 2 1
    // scc1 = 4 6 5
    // scc2 = 7

    return 0;
}
