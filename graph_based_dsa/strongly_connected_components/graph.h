#ifndef GRAPH_H
#define GRAPH_H

// Kosaraju's algorithm to find strongly connected components in C++

#include <iostream>
#include <stack>
#include <vector>
#include <algorithm>
#include <stdexcept>
#include <sstream>

class Graph {
    std::vector<std::vector<int>> adj;
    void fillOrder(int v, std::vector<bool> &visited, std::stack<int> &stack);
    void DFS(int v, std::vector<bool> &visited);
public:
    Graph(int size);
    void addEdge(int v, int e);
    void printSCC();
    Graph transpose();
};

Graph::Graph(int size) {
    adj.resize(size);
}

void Graph::addEdge(int v, int e) {
    auto size = adj.size();

    if (v < 0 || v >= size || e < 0 || e >= size) {
        std::stringstream ss;
        ss << "out of range: 0-" << size - 1;
        throw std::out_of_range(ss.str());
    }

    adj[v].push_back(e);
}

Graph Graph::transpose() {
    Graph g(adj.size());

    for (int v = 0; v < adj.size(); ++v) {
        for (auto e : adj[v]) {
            g.adj[e].push_back(v);
        }
    }

    return g;
}

void Graph::fillOrder(int v, std::vector<bool> &visited, std::stack<int> &stack) {
    visited[v] = true;

    for (auto e : adj[v]) {
        if (!visited[e]) {
            fillOrder(e, visited, stack);
        }
    }

    stack.push(v);
}

void Graph::DFS(int v, std::vector<bool> &visited) {
    visited[v] = true;
    std::cout << v << " ";

    for (auto e : adj[v]) {
        if (!visited[e]) {
            DFS(e, visited);
        }
    }
}

void Graph::printSCC() {
    std::stack<int> stack;

    std::vector<bool> visited(adj.size(), false);

    for (int v = 0; v < adj.size(); ++v) {
        if (!visited[v]) {
            fillOrder(v, visited, stack);
        }
    }

    Graph gr = transpose();

    std::fill(visited.begin(), visited.end(), false);

    while (!stack.empty()) {
        int s = stack.top();
        stack.pop();

        if (!visited[s]) {
            gr.DFS(s, visited);
            std::cout << std::endl;
        }
    }
}

#endif // GRAPH_H
