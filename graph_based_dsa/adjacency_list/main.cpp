#include "graph.h"
#include <iostream>

using namespace std;

int main()
{
    Graph g(5);

    g.addEdge(0, 1);
    g.addEdge(0, 2);
    g.addEdge(0, 3);
    g.addEdge(1, 2);

    g.printGraph();

    // Vertex 0:-> 1-> 2-> 3
    // Vertex 1:-> 0-> 2
    // Vertex 2:-> 0-> 1
    // Vertex 3:-> 0
    // Vertex 4:

    g.removeEdge(0, 1);
    g.removeEdge(1, 2);

    g.printGraph();

    return 0;
}
