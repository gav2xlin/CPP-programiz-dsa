#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <vector>
#include <algorithm>

class Graph {
private:
    std::vector<std::vector<int>> adjList;
public:
    Graph(int numVertices) {
        adjList.resize(numVertices);
    }

    void addEdge(int v, int e) {
        adjList.at(v).push_back(e);
        adjList.at(e).push_back(v);
    }

    void removeEdge(int v, int e) {
        auto itv = std::find(adjList.at(v).begin(), adjList.at(v).end(), e);
        auto ite = std::find(adjList.at(e).begin(), adjList.at(e).end(), v);
        if (itv != adjList[v].end() && ite != adjList[e].end()) {
            adjList[v].erase(itv);
            adjList[e].erase(ite);
        }
    }

    void printGraph() {
        for (int v = 0; v < adjList.size(); ++v) {
            std::cout << "\n Vertex " << v << ":";
            for (auto e : adjList[v]) {
                std::cout << "-> " << e;
            }
            std::cout << "\n";
        }
    }
};

#endif // GRAPH_H
