#include "graph.h"
#include <iostream>

using namespace std;

int main()
{
    Graph g(4);

    g.addEdge(0, 1);
    g.addEdge(0, 2);
    g.addEdge(1, 2);
    g.addEdge(2, 0);
    g.addEdge(2, 3);

    g.printGraph();

    // 0 : 0 1 1 0
    // 1 : 1 0 1 0
    // 2 : 1 1 0 1
    // 3 : 0 0 1 0

    return 0;
}
