#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <vector>
#include <stdexcept>
#include <sstream>

class Graph {
private:
    std::vector<std::vector<bool>> adjMatrix;
public:
    Graph(int numVertices) {
        adjMatrix.resize(numVertices, std::vector<bool>(numVertices, false));
    }

    void addEdge(int v, int e) {
        auto size = adjMatrix.size();

        if (v < 0 || v >= size || e < 0 || e >= size) {
            std::stringstream ss;
            ss << "out of range: 0-" << size - 1;
            throw std::out_of_range(ss.str());
        }

        adjMatrix[v][e] = true;
        adjMatrix[e][v] = true;
    }

    void removeEdge(int v, int e) {
        auto size = adjMatrix.size();

        if (v < 0 || v >= size || e < 0 || e >= size) {
            std::stringstream ss;
            ss << "out of range: 0-" << size - 1;
            throw std::out_of_range(ss.str());
        }

        adjMatrix[v][e] = false;
        adjMatrix[e][v] = false;
    }

    void printGraph() {
        for (int i = 0; i < adjMatrix.size(); i++) {
            std::cout << i << " : ";
            for (int j = 0; j < adjMatrix[i].size(); j++) {
                std::cout << adjMatrix[i][j] << " ";
            }
            std::cout << "\n";
        }
    }
};

#endif // GRAPH_H
