#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include <iostream>
#include <list>

class Graph {
    std::vector<std::vector<int>> adjList;
public:
    Graph(int numVertices);
    void addEdge(int src, int dest);
    void BFS(int vertex);
};

Graph::Graph(int numVertices) {
    adjList.resize(numVertices);
}

void Graph::addEdge(int src, int dest) {
    adjList[src].push_back(dest);
    adjList[dest].push_back(src);
}

void Graph::BFS(int vertex) {
    std::vector<bool> visited(adjList.size(), false);

    std::list<int> queue;

    visited[vertex] = true;
    queue.push_back(vertex);

    while (!queue.empty()) {
        int e = queue.front();
        std::cout << e << " ";
        queue.pop_front();

        for (auto e : adjList[e]) {
            if (!visited[e]) {
                visited[e] = true;
                queue.push_back(e);
            }
        }
    }
}

#endif // GRAPH_H
