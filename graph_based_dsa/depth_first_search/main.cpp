#include "graph.h"
#include <iostream>

using namespace std;

int main()
{
    Graph g(4);
    g.addEdge(0, 1);
    g.addEdge(0, 2);
    g.addEdge(1, 2);
    g.addEdge(2, 3);

    g.DFS(2);

    // 2 3

    return 0;
}
