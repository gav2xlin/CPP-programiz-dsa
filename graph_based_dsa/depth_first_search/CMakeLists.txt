cmake_minimum_required(VERSION 3.5)

project(depth_first_search LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(depth_first_search main.cpp graph.h)

install(TARGETS depth_first_search
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
