#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include <iostream>

class Graph {
    std::vector<std::vector<int>> adjList;
    std::vector<bool> visited;
public:
    Graph(int numVertices);
    void addEdge(int src, int dest);
    void DFS(int vertex);
};

Graph::Graph(int numVertices) {
    adjList.resize(numVertices);
    visited.resize(numVertices, false);
}

void Graph::addEdge(int src, int dest) {
    adjList[src].push_back(dest);
}

void Graph::DFS(int v) {
    visited[v] = true;

    std::cout << v << " ";

    std::vector<int>::iterator i;
    for (auto e : adjList[v]) {
        if (!visited[e]) {
            DFS(e);
        }
    }
}

#endif // GRAPH_H
